"""
Attach members
===============

Link a list of members on a page to a list of LRI members id
"""

from pelican import signals


def set_for_page(page, generator):
    if hasattr(page, "members"):
        members = page.members.split(",")
        members = [m.strip() for m in members]
        page.memberlist = members

def set_members_id(generator):
    for page in generator.pages:
        set_for_page(page, generator)
    for page in generator.hidden_pages:
        set_for_page(page, generator)

def set_members_id_articles(generator):
    for article in generator.articles:
        set_for_page(article, generator)

def register():
    signals.page_generator_finalized.connect(set_members_id)
    signals.article_generator_finalized.connect(set_members_id_articles)
