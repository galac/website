"""
Edit Page Link
===============

Adds a `edit_page_link` attribute to pages
"""

from pelican import signals

def set_for_page(page, generator):
    source = page.source_path
    i = source.rfind("content") # we use the last occurence of content in the sourcepath
    if  generator.settings.has_key("EDITLINKBASE"):
        base = generator.settings["EDITLINKBASE"]
    else:
        base = ""
    page.edit_link = base + source[i:]

def set_edit_link_pages(generator):
    for page in generator.pages:
        set_for_page(page, generator)

    for page in generator.hidden_pages:
        set_for_page(page, generator)


def set_edit_link_articles(generator):
    for article in generator.articles:
        set_for_page(article, generator)

def register():
    signals.page_generator_finalized.connect(set_edit_link_pages)
    signals.article_generator_finalized.connect(set_edit_link_articles)
