# Edit Page Link Plugin

This plugin adds a parameter *edit_link* to pages to allow for an edition link
to be added on pages.

## Configutation

For the plugin to work, you need to specify a *EDITLINKBASE* on your confirguation
file. By default, the *EDITLINKBASE* is empty which is not likely to work...

The final link is made from *EDITLINKBASE* and the source path of your file (taken
from the *source_path* parameter of the page).

## Example

If your source file are stored on a github project, your *EDITLINKBASE* is likely 
to be

    EDITLINKBASE = "https://github.com/[USER NAME]/[Project]/edit/master/"

which will give an edit link of

    "https://github.com/[USER NAME]/[Project]/edit/master/content/pages/mypage.md"
