"""
Articles on page
=================

Allows a list of articles to be attached to a page

"""

from pelican import signals
from datetime import datetime
import pytz

def set_for_page(page, article_generator):
    page_articles= None
    if hasattr(page, "include_category"):
        page_category = page.include_category
        if not hasattr(page,"number_articles"):
            page.number_articles = 3
        if not hasattr(page,"number_upcomming"):
            page.number_upcomming = 3
        c = int(page.number_articles)
        c2 = int(page.number_upcomming)
        if page_category != "all":
            for category, articles in article_generator.categories:
                if category == page_category:
                    page_articles = articles
                    page.category_url = category.url
                    break
        else:
            page_articles = article_generator.articles
            page.category_url = article_generator.settings["INDEX_SAVE_AS"]
    if hasattr(page, "include_tag"):
        page_tag = page.include_tag
        if not hasattr(page,"number_articles"):
            page.number_articles = 3
        if not hasattr(page,"number_upcomming"):
            page.number_upcomming = 3
        c = int(page.number_articles)
        c2 = int(page.number_upcomming)

        for tag in article_generator.tags:
            if tag == page_tag:
                page_articles = article_generator.tags[tag]
                page.category_url = tag.url
                break


    if page_articles is not None:
        page.articles = []
        page.articles_upcoming = []
        now = None
        for article in page_articles:
            if now is None:
                now = datetime.now()
                now = article.date.tzinfo.localize(now)
            if article.date >= now:
                page.articles_upcoming.append(article)
            else:
                page.articles.append(article)

        page.articles = page.articles[:c]
        page.articles_upcoming.reverse()
        page.articles_upcoming = page.articles_upcoming[:c2]



def set_articles_on_pages(generators):
    article_generator = generators[0]
    page_generator = generators[1]
    for page in page_generator.pages:
        set_for_page(page, article_generator)

    for page in page_generator.hidden_pages:
        set_for_page(page, article_generator)

def register():
    signals.all_generators_finalized.connect(set_articles_on_pages)
