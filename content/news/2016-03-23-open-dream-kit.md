Title: Starting of the OpenDreamKit Project
date: 2016-03-23
slug: 2016-03-23-open-dream-kit
Authors: Viviane Pons
lang: en
tags: combinatorics
members: Nicolas Thiéry, Viviane Pons, Florent Hivert, Erik Bray, Jeroen Demeyer, Benoit Pilorget
img:odk-logo.svg

[OpenDreamKit](http://opendreamkit.org/) is a European project which was started 
in September 2015 and will run for four years. It includes 15 partner institutions 
in Europe and should provide substantial funding to the open source computational 
mathematics ecosystem, in particular the software [Sage](pages/sage.html). It involves 
many members of GALaC. Indeed, it has been initiated by  [Nicolas Thiéry](http://nicolas.thiery.name/) 
who is now the coordinator of project. 

