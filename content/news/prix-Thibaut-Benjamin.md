Title: Prix pour le travail de stage de Thibaut Benjamin encadré par Florent Hivert
date: 2016-07-31
slug: 2016-07-31-prix-Thibaut-Benjamin
Authors:  Florent Hivert
lang: en
tags: combinatorics,seminar
 
L'école polytechnique à attribué à Thibaut Benjamin étudiant de 3ème année un
prix du stage de recherche pour son travail sur

«Une Formalisation des Représentations des Groupes Symétriques en Coq/SSReflect»

Ce travail comporte à la fois une partie théorique (lecture d'article et
exposés à des groupes de travail de recherche) et une partie d'implantation.
Le résultat principal est une preuve formelle de l'isomorphisme de Frobenius
qui envoie le produit d'induction des fonctions centrales de S_m x S_n vers
S_(m+n) et le produit des fonctions symétriques. Il consiste en environ 3800
lignes de code disponible en ligne sur https://github.com/ThiBen/ReprSymGroup.


Voici son rapport : [le rapport](/documents/RapportThibaut.pdf) 
