Title:  Attribution Accessit du prix de thèse 2018 Graphes Charles DELORME
date: 2018-09-30
slug: 2018-09-30-prix-George
Authors:  George Manoussakis
lang: en
tags: graphs, seminar
 
 Nous avons le plaisir de vous annoncer que l'étudiant George Manoussakis  encadré par Johanne Cohen et Antoine Deza a reçu l'accessit du prix de thèse 2018
 Graphes Charles DELORME.
 
http://gtgraphes.labri.fr/pmwiki/pmwiki.php/PrixTheseDelorme/PrixTheseDelorme

Voici le résumé de sa thèse : Nous introduisons d'abord la classe des graphes k-dégénérés qui est souvent utilisée pour modéliser des grands graphes épars issus du monde réel. Nous proposons de nouveaux algorithmes d'énumération pour ces graphes. En particulier, nous construisons un algorithme énumérant tous les cycles simples de tailles fixés dans ces graphes, en temps optimal. Nous proposons aussi un algorithme dont la complexité dépend de la taille de la solution pour le problème d'énumération des cliques maximales de ces graphes. Dans un second temps nous considérons les graphes en tant que systèmes distribués et nous nous intéressons à des questions liées à la notion de couplage lorsqu’aucune supposition n’est faite sur l'état initial du système, qui peut donc être correct ou incorrect. Dans ce cadre nous proposons un algorithme retournant une deux tiers approximation du couplage maximum.Nous proposons aussi un algorithme retournant un couplage maximal quand les communications sont restreintes de telle manière à simuler le paradigme du passage de message. Le troisième objet d'étude n'est pas directement lié à l'algorithmique de graphe, bien que quelques techniques classiques de ce domaine soient utilisées pour obtenir certains de nos résultats. Nous introduisons et étudions certaines familles de polytopes, appelées Zonotopes Primitifs, qui peuvent être décrits comme la somme de Minkowski de vecteurs primitifs. Nous prouvons certaines propriétés combinatoires de ces polytopes et illustrons la connexion avec le plus grand diamètre possible de l'enveloppe convexe de points à coordonnées entières à valeurs dans [k], en dimension d. Dans un second temps, nous étudions des paramètres de petites instances de Zonotopes Primitifs, tels que leur nombre de sommets, entre autres. 


Pour plus d'information sur les travaux de George, allez sur la page web https://tel.archives-ouvertes.fr/tel-01835110v1
