Title:  The Women In Sage workshop
date: 2017-04-07
slug: 2017-04-07-womeninsage
Authors:  Viviane Pons
lang: en
tags: combinatorics, sage
 
Last January, Viviane Pons organized the first Women In Sage event in Europe as part
of the [OpenDreamKit](http://opendreamkit.org/) project. On the project website,
you will find [a full report](http://opendreamkit.org/2017/04/06/WomenInSage/) of the event 
describing its impact on the Sage community.
