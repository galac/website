// Author: Nathann Cohen

window.onload = function() {
    x=$("#whatever");
    jQuery.getJSON("https://api.archives-ouvertes.fr/search/?"+
		   "&fq=collIsChildOfColl_fs:LRI-GALAC*"+
		   "&rows=20000"+
		   "&fq=producedDateY_i:[2014 TO *]"+
		   "&wt=json"+
		   "&group.field=producedDateY_i"+
		   "&group.limit=500"+
		   "&group=true"+
		   "&fq=docType_s:(ART OR COMM OR POSTER OR PRESCONF OR OUV OR COUV OR DOUV OR PATENT OR OTHER OR UNDEFINED OR THESE OR HDR OR MEM OR LECTURE OR NOTE OR OTHERREPORT)"+
		   "&sort=producedDate_tdate desc"+
		   "&fl=title_s,authFullName_s,halId_s,producedDateY_i,journalTitle_s,conferenceTitle_s,docType_s,country_s",

		   function(data){
		       data=data.grouped.producedDateY_i.groups;
		       x.html(""); // remove "loading" text

		       data.forEach(function(year){
			   yearblock=$("#template > .yearblock").clone();
                           yearblock.find("text.year").html(year.groupValue);
			   yearblock.find("text.category_name").html(function (x,y) { return y+" "+year.groupValue;});

			   year.doclist.docs.forEach(function (y){

			       entry=$("#template > .bibentry").clone();
			       entry.children(".title").html(y.title_s[0]);
			       entry.children(".title").attr("href","http://hal.archives-ouvertes.fr/"+y.halId_s);
			       entry.children("a.bibtex").attr("href","http://hal.archives-ouvertes.fr/"+y.halId_s+"/bibtex");
			       entry.children(".journal").html(y.journalTitle_s || y.conferenceTitle_s || " ");
			       entry.children(".authors").html(y.authFullName_s.join(", "));
			       appropriate_block = ({"ART":"journals","COMM":"confs"}[y.docType_s] || "others");
			       yearblock.find("div."+appropriate_block)
				   .css("display","block")
				   .children("div").append(entry);
			   });

			   x.append(yearblock);
		       });
		   });
};
