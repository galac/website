Title: Plateau Saclay Algorithms Seminar
slug: plateau-seminar
description: Seminar of Algorithmic of the Saclay Plateau
lang: en
page_order: 3
include_tag: Plateau seminar
parentpage: seminars

The Plateau Saclay Algorithms Seminar is held every other Friday afternoon in LIX.
This working group is partially supported by Labex DigiCosme (Digital worlds: 
distributed data, programs and architectures).

If you do  wish (or not) to receive any emails from this seminar,
you can subscribe or unsubscribe from the mailing [list](https://sympa.lix.polytechnique.fr/lists/subscribe/sem-algo-saclay).
 
Subscribe to sympa@lix.polytechnique.fr mailing list  with the following title 
"SUBSCRIBE sem-algo-saclay@lix.polytechnique.fr" LastName FirstNale

Unsubscribe to sympa@lix.polytechnique.fr mailing list  with the following title 
with the following title  "UNSUBSCRIBE sem-algo-saclay@lix.polytechnique.fr"
  
 
 

## Recent and up-coming seminars

<!-- the list of seminars come from the seminar blog posts -->
<!-- Add a "number_articles" variable to change the number of seminars printed (default is 3) -->
