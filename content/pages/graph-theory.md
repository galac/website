Title: Graph Theory
slug: graph-theory
description: Research activities of the GALaC team in Graph Theory.
lang: en
page_order: 3
parentpage: research
include_tag: graphs
thumb_img: graphs-thumb.svg
img: graphs.png

The main focus is on structural and algorithmic point of views.
The team established expertise includes problems such as finding large cycles in a given graph, graph
colorings, covering problems, and extremal graph theory. For example, some team members are particularly
interested in Thomassen’s conjecture: Every 4-connected line graph is Hamiltonian. Finding sufficient
and computationally tractable conditions for a graph to be Hamiltonian is of significant importance from
both theoretical and algorithmic viewpoints as Hamiltonicity is an NP-hard problem.

Generalization of such problems has also been recently considered for edge- or vertex-colored graphs.
For example, one may look for properly colored spanning trees in an edge- or a vertex-colored graph.
Alternatively, one may look for a dominating set in a vertex colored graph having at least one vertex
from each color. Beside their theoretical interest, these extensions have applications in areas including
biocomputing and web problems.

Many of the questions we consider can be stated in terms of (integer) linear optimization that is an
expertise of new members of the team with research interests focusing on the combinatorial, computational,
and geometric aspects of linear optimization. In this regard the aim would be to investigate recent
results illustrating the significant interconnection between the most computationally successful algorithms
for linear optimization and its generalizations, and the geometric and combinatorial structure of the input.
Ideally, the deeper theoretical understanding will ultimately lead to increasingly efficient algorithms.
Most of our research collaborations involve French research groups including LaBRI, LIRMM, LIAFA, and
LIMOS as well as research groups in Europe, North America, China, Japan, India and South America.
