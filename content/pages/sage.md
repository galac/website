Title: SageMath, an open-source mathematical software
menutitle: Sage
slug: sage
parentpage: softwares
lang: en
img: sage.png
description: Sage is an open source mathematical software to which the GALaC team contributes a lot.
page_order: 2
members: Nicolas Thiéry, Florent Hivert, Viviane Pons, Joël Gay, Erik Bray, Jeroen Demeyer

[Sage](http://www.sagemath.org/) is an open-source mathematical software for research and teaching in algebra, geometry, arithmetic, number theory, combinatorics, cryptography, scientific computation and many other domains. The development model is based on openness, sharing, and collaboration. The main objective is to create a viable alternative to Maple, Mathematical, Magma and MATLAB.

Many members of GALaC are active contributors especially in [combinatorics](https://wiki.sagemath.org/combinat) but also as core developers of the software. Since 2016, GALaC employs two developers as research engineers working essentially on Sage. They are part of the [OpenDreamKit](http://opendreamkit.org/) project lead by Nicolas Thiéry.


## GALaC members involved in Sage

<!-- the list of members is taken from the members parameter -->
