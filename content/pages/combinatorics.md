Title: Combinatorics
slug: combinatorics
description: Research activities of the GALaC team in Combinatorics.
lang: en
page_order: 2
parentpage: research
include_tag: combinatorics
thumb_img: combinatorics-thumb.svg
img: combinatorics.png

The main focus of this activity is the interrelation between algebraic structure and algorithms. We plan
to work on the following subjects:

- Algebraic structures (Combinatorial Hopf Algebras, Operads, Monoids, ...) related to algorithms;
- Enumerative combinatorics and symbolic dynamic.
- Object oriented software design for modeling mathematics and development of [SageMath](sage.html);

More precisely, the research project takes place in effective algebraic combinatorics, at the interface
of enumerative combinatorics and analysis of algorithms on one hand and symbolic and algebraic computation
on the other hand. The objective is twofold: firstly, thanks to vast generalization of the notion of
generating series, we hope to give a theoretical framework allowing to study the fine behavior of various
algorithms. Reciprocally, the study of those very same algorithms gives a new mean to discover algebraic
identities. Those identities have many applications in mathematics, in particular in representation theory
but also in physics (mainly statistical physics).

The research relies deeply on computer experimentation and contains as a consequence an important
software development part within the [Sage-Combinat](https://wiki.sagemath.org/combinat) software project. However, the required level
of sophistication, flexibility, and breath of computational tools is reaching a point where large scale collaborative
development is critical. The design and collaborative development of such a software is raising
research-grade computer science challenges around the modelling of mathematics, the management
of large hierarchy of (object oriented) classes, etc.

Those very specific questions also raise more general combinatorial questions. We therefore plan to
work on enumerative combinatorics and cellular automaton, in particular on trees.
This activity is conducted with close collaborators in France, Germany, North America, and India.
