Title: Plateau Saclay Combinatorics Seminar
slug: combi-seminar
description: Seminar of Combinatorics of the Saclay Plateau
lang: en
page_order: 2
include_tag: Combi seminar
parentpage: seminars
number_upcomming: 3
number_articles: 3

<a href="http://www.lix.polytechnique.fr/" ><img class="img-responsive page-image" src="/images/logo-lix.png" alt="LIX" /></a>

The Plateau Saclay Combinatorics Seminar is held on several Mondays at 11am in room Philippe Flajolet (top floor on the left) at [LIX](https://www.lix.polytechnique.fr/article/8/view). It is co-organized by the [Combi team of LIX](http://www.lix.polytechnique.fr/combi) and the GALaC team.

The mailing list for this seminar is combi_lix_lri@services.cnrs.fr. [Subscribe to this list](mailto:sympa@services.cnrs.fr?Subject=subscribe%20combi_lix_lri@services.cnrs.fr%20Firstname%20Lastname) by sending an email to [sympa@services.cnrs.fr](mailto:sympa@services.cnrs.fr) with object "subscribe combi_lix_lri@services.cnrs.fr Firstname Lastname", if you want to receive the announcements.

If you are interested in giving a talk, feel free to contact the organizers:

 * [Vincent Pilaud (LIX)](http://www.lix.polytechnique.fr/~pilaud/)
 * [Viviane Pons (LRI)](https://www.lri.fr/~pons/)

*The seminar archives (2006 -- 2017) can be found on the [previous page](https://www.lix.polytechnique.fr/combi/archive.html)*.
 

## Recent and up-coming seminars

<!-- the list of seminars come from the seminar blog posts -->
<!-- Add a "number_articles" variable to change the number of seminars printed (default is 3) -->
