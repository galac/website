Title: Seminars
slug: seminars
description: Seminars of the GALaC team
lang: en
page_order: 5
include_category: seminars

GALaC organizes or participates in three different regular seminars.

<!-- the list of seminars come from the seminar blog posts -->
<!-- Add a "number_articles" variable to change the number of seminars printed (default is 3) -->
