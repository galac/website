Title: Logiciels
menutitle: Tous les logiciels
dropdowntitle: Logiciels
slug: softwares
lang: fr
description: Les logiciels développés par les membres de l'équipe GALaC
category: software

L'équipe GALaC est impliquée dans le développement de nombreux logiciels.

<!-- List of software from the "softaware" pages -->
