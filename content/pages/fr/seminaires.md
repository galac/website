Title: Seminaires
slug: seminars
description: Seminaires de l'équipe GALaC
lang: fr
page_order: 4
include_category: seminars

L'équipe GALaC organise ou participe à trois séminaires réguliers.


<!-- the list of seminars come from the seminar blog posts -->
<!-- Add a "number_articles" variable to change the number of seminars printed (default is 3) -->
