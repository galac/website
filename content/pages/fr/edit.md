Title: Comment éditer
slug: edit
lang: fr
status:hidden

Le contenu du site est généré par le logiciel [Pelican](http://getpelican.com/) à
partir de fichiers sources partagés sur un répertoire git. Ce répertoire est public 
accessible à [cette adresse](https://gitlri.lri.fr/galac/website) sur un serveur 
gitlab hébergé par le LRI. Seuls les membres du LRI peuvent se connecter au serveur 
gitlab.

**Je suis membre de GALaC et je souhaite apporter une modification au site, comment faire ?**

[Tout est expliqué ici](https://gitlri.lri.fr/galac/website).
