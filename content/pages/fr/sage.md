Title: SageMath, un logiciel mathématique open-source
menutitle: Sage
slug: sage
parentpage: softwares
lang: fr
img: sage.png
description: Sage est un logiciel de mathématique open-source auquel contribue l'équipe GALaC
page_order: 2
members: Nicolas Thiéry, Florent Hivert, Viviane Pons, Joël Gay, Erik Bray, Jeroen Demeyer

[Sage](http://www.sagemath.org/) est un logiciel mathématique libre destiné à la recherche et à l’enseignement en algèbre, géométrie, arithmétique, théorie des nombres, combinatoire, cryptographie, calcul scientifique et dans d’autres domaines apparentés. Le modèle de développement de Sage comme ses caractéristiques techniques se distinguent par un souci extrême d’ouverture, de partage, de coopération et de collaboration : notre but est de construire la voiture, non de réinventer la roue. L’objectif général de Sage est de créer une alternative libre viable à Maple, Mathematica, Magma et MATLAB.

De nombreux membres de l'équipe GALaC sont des contributeurs réguliers à Sage en
particulier dans le domaine de la [combinatoire]((https://wiki.sagemath.org/combinat),
mais aussi en tant que core-developers. Depuis 2016, GALaC emploie deux développeurs Sage
en tant qu'ingénieurs de recherche. Ils font partie du projet [OpenDreamKit](http://opendreamkit.org/)
mené par Nicolas Thiéry.


## Les membres GALaC impliqués dans Sage

<!-- the list of members is taken from the members parameter -->
