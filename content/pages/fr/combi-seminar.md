Title: Séminaire de Combinatoire du Plateau de Saclay
slug: combi-seminar
description: Séminaire de Combinatoire du Plateau de Saclay
lang: fr
page_order: 2
include_tag: Combi seminar
parentpage: seminars
number_upcomming: 3
number_articles: 3

<a href="http://www.lix.polytechnique.fr/" ><img class="img-responsive page-image" src="/images/logo-lix.png" alt="LIX" /></a>

Le séminaire combinatoire du plateau de Saclay est organisé un lundi sur deux à 15 h conjointement par l'équipe du [Combi](http://www.lix.polytechnique.fr/combi) du LIX et l'équipe GALaC. Si vous souhaitez donner un exposé ou recevoir les annonces, merci de contacter l'un des organisateurs :

* [Marie Albenque (LIX)](http://www.lix.polytechnique.fr/~albenque/)
* [Vincent Pilaud (LIX)](http://www.lix.polytechnique.fr/~pilaud/)
* [Viviane Pons (LRI)](https://www.lri.fr/~pons/)
 
La liste de diffusion du séinaire est combi_lix_lri@services.cnrs.fr. [Pour vous inscrire](mailto:sympa@services.cnrs.fr?Subject=subscribe%20combi_lix_lri@services.cnrs.fr%20Firstname%20Lastname) envoyez un mail à [sympa@services.cnrs.fr](mailto:sympa@services.cnrs.fr) avec comme objet "subscribe combi_lix_lri@services.cnrs.fr Prénom Nom", et vous recevrez les annonces hebdomadaires.

*Les archives du séminaires (2006 -- 2017) sont disponibles sur [l'ancienne page](http://www.lix.polytechnique.fr/combi/#seminaire)*


## Séminaires récents et à venir

<!-- the list of seminars come from the seminar blog posts -->
<!-- Add a "number_articles" variable to change the number of seminars printed (default is 3) -->
