Title: Equipe GALaC du LRI, Paris-Sud
TitleSEO: Equipe GALaC du LRI, Paris-Sud (Orsay, France)
menuTitle: Accueil
lang: fr
slug:home
page_order: 1
URL:
save_as: index.html
include_category: all
description: GALaC est une équipe de recherche du LRI, université Paris-Sud. Nos thèmes de recherche sont la théorie des graphes, la combinatoire, l'algorithmique et les systèmes de réseaux distribués.

L'équipe GALAC rassemble les chercheurs du [LRI](https://www.lri.fr/) qui travaillent sur des thématiques de combinatoire, d'algorithmique, de théorie des graphes, de systèmes en réseaux et distribués.

Plus précisément, nos domaines de recherche sont les suivants : notre recherche en combinatoire porte sur les fortes interactions et relations existant entre les algorithmes et les structures algébriques, et la recherche en théorie des graphes sur des propriétés structurelles et des problèmes de décomposition. Des algorithmes et modèles efficaces pour les systèmes en réseaux sont développés dans la troisième activité de l'équipe, en utilisant le formalisme de la théorie des jeux et du calcul distribué.


Voici une présentation des activités de l'équipe GALAC foit en 2013 pour l'AERES : [transparent AERES 2013]({filename}/documents/lri-slides-2013.pdf) et [projet de recherche]({filename}/documents/LRIreport-GALAC.pdf).


## Nouvelles récentes

<!-- print the list of blog post because of the "include_category" parameter -->
<!-- Add a "number_articles" variable to change the number of posts printed (default is 3) -->

