Title: Théorie des graphes
slug: graph-theory
description: Activités de recherche dans GALaC en théorie des graphes.
lang: fr
page_order: 3
parentpage: research
include_tag: graphs
thumb_img: graphs-thumb.svg
img: graphs.png

Le sujet principal est une point de vue structurel et algorithmique.
L'équipe a établi une expertise comprennant les problèmes tel que trouver les grands cylcles d'un graphe donné,
colorier un graphe, résoudre des problèmes de couverture, ou faire avancer la théorie des graphes en trouvant les graphes extrèmes
répondant à une contrainte.

La généralisation de quelques problèmes est aussi considérée pour les graphes arêtes ou sommets colorés.
Par exemple, il a été étudié les graphes couvrants colorés pour des graphes arêtes ou sommets colorés.
De manière alternative il a été recherche l'ensemble dominant dans un graphe ayant au moins un sommet de chaque couleur.
au delà de l'intérêt purement théoriques ces démarches ont un grand intérêt aussi bien dans le domaine de la bioinformatique que dans celui du Web.

Bon nombre des questions que nous considérons peuvent aussi être déclarée en termes d'optimisation de linéaire.
Ce qui ouvre des persepectives.

Nous avons de nombreuses collaborations avec les groupes français :  LaBRI, LIRMM, LIAFA et
LIMOS aussi bien qu'en Europe, en Amérique du nord et du sud et principalement en Asie avec la Chine, le Japan, l'Inde.
