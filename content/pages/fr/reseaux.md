Title: Algorithmes pour les systèmes en réseaux
slug: network
description: Activités de recherche dans GALaC sur les systèmes en réseaux
lang: fr
page_order: 1
parentpage: research
include_tag: network
thumb_img: and-thumb.png
img: and.png

Le but principal de ce groupe est de concevoir, modéliser, étudier le controle et les performances des algorithmiques conçus spéficiquement pour les systèmes répartis et leurs applications. 
La contribution scientifique que nous visons est à la fois théorique avec le développement de nouveaux modèles mathématiques 
et des preuves de qualité et mais aussi bien appliquée avec le développement d'outils innovants pour différents types de réseaux 
(opportunistes, centrés sur le contenu ou congitifs).

Plus précisément les objectifs du group ANS (pour Algorithms for Networked systems) pour les 5 prochaines années sont 

- D'établir des briques de bases pour la conception et l'optimisation des systèmes en réseaux. 
Ceci inclue la théorie du controle, la théorie des jeux, les algorithmes répartis et particulièrement l'auto-stabilisation 
et plus généralement la tolérance aux fautes aussi bien que la simulation de systèmes via des modèles à évenements discrets.
- De concevoir des algorithmes efficaces et des protocoles basés sur le développement des trames théoriques, d'en evaluer les performance grace à des scénarii pratiques. Ceci inclue les réseaux sans fils opportunistes (entre autre réseaux de robots, réseau sans fils ad hoc, réseaux de capteurs), les futures infrasctrutures et protocoles pour l'internet (information, Réseau centrés sur le contenus), la sécurité et la sureté dans les systemèmes cyber-physiques. 

Les collaborations de cet axe prennent place sur les 5 continents. 

Voici une présentation des activités de l'équipe GALAC foit en 2013 pour l'AERES : [transparent AERES 2013]({filename}/documents/lri-slides-2013.pdf) et [projet de recherche]({filename}/documents/LRIreport-GALAC.pdf).
