Title: Combinatoire
slug: combinatorics
description: Activités de Recherche dans GALaC en combinatoire
lang: fr
page_order: 2
parentpage: research
include_tag: combinatorics
thumb_img: combinatorics-thumb.svg
img: combinatorics.png

L'intérêt principal de cette activité est l'étude des relations entre les structures algébriques et les algorithmes. 
Les chercheurs s'attachent particulirement aux sujets suivants: 

- les structures algébriques (combinatoire des algèbres de Hopf, Opérades, Monoides, ...) relatives aux algorithmes; 
- la combinatoire énumérative et la dynamique symbolique. 
- Les logiciels orientés objets conçus pour la modélisation des mathématiques, en particulier le développement du logiciel [SageMath](sage.html);

Plus précisément, les projets de recherches relèvent de la combinatoire algébrique, sont à l'interface de la combinatoire énumérative et 
concernent l'analyse d'algorithmes d'un point de vue des calculs symboliques et algébriques ou de calcul algébriques. 
Les objectifs sont doubles: d'abord, grâce à une généralisation masive de la notion de série génératrice nous espérons proposer un canevas théorique permettant l'étude du comportement fin de nombreux et différents algorithmes et ensuite et de manière réciproque l'étude des même algorithmes ouvre de nouvelles pistes pour la découverte d'objets ou d'identités algébriques d'intérêt. 
Ces identités ont plusieurs applications en mathématiques, en particulier dans la théorie des représentations mais aussi en physique (principalement en physique statistique).


Les recherches reposent largement sur l'expérimentation par ordinateur, il s'en suit une part importante de développement via le projet logiciel [Sage-Combinat](https://wiki.sagemath.org/combinat).

Cependant, le niveau de sophistication, la souplesse et la qualité des outils de calcul requis atteint un point où à grande échelle le développement collaboratif est essentiel. 
La conception et le développement collaboratif d'un tel logiciel soulève la recherche de qualité.
Les défis sont tant du domaine de l'informatique qu'autour de la modélisation mathématique et de la gestion d'un grande hiérarchie de (orientée objet) classes, etc.

Ces questions spécifiques posent aussi de manière plus générale des questions combinatoires.
Il est alors envisager un travail sur la combinatoire enumérative, les automates cellulaires en particulier les arbres. 

Cet axe nourrit des collaborations régulières en France mais aussi avec l'Allemagne, l'Amérique du nord et l'Inde.
