Title: Séminaire d'Algorithmique du plateau de Saclay
slug: plateau-seminar
description: Séminaire d'Algorithmique du plateau de Saclay
lang: fr
page_order: 3
include_tag: Plateau seminar
parentpage: seminars

Le séminaire d'algorithmique du plateau de Saclay est organisé toutes les deux semaines,
le vendredi après-midi au LIX.


## Séminaires récents et à venir

<!-- the list of seminars come from the seminar blog posts -->
<!-- Add a "number_articles" variable to change the number of seminars printed (default is 3) -->
