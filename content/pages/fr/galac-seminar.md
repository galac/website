Title: Séminaire d'équipe GALaC
slug: team-seminar
description: Le séminaire d'équipe de GALaC
lang: fr
page_order: 1
include_tag: Team seminar
parentpage: seminars

Le séminaire d'équipe de GALaC est organisé régulièrement le vendredi à 14h00 dans le bâtiment PCRI (650) au LISN.


## Séminaires récents et à venir

<!-- the list of seminars come from the seminar blog posts -->
<!-- Add a "number_articles" variable to change the number of seminars printed (default is 3) -->
