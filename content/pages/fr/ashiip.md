Title: aSHIIP, générateur de topologies aléatoires avec hierarchie.
menutitle: aSHIIP
slug: ashiip
parentpage: softwares
lang: fr
description: aSHIIP est un générateur de topologies aléatoires avec hierarchie.
page_order: 1
members: Marc-Antoine Weisser, Joanna Tomasik

[aSHIIP](http://wwwdi.supelec.fr/software/ashiip) est un générateur de topologies 
aléatoires avec hierarchie.


## Les membres GALaC impliqués dans aSHIIP

<!-- the list of members is taken from the members parameter -->
