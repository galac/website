Title: Recherche
slug: research
description: La recherche dans l'équipe GALac
lang: fr
page_order: 3

L'équipe GALaC a trois principaux thèmes de recherche.

![Graphs Algorithmic and Combinatorics](/images/GalacEnImageTer2016_small.png){ .center .img-responsive}

Une présentation générale des activités de recherche dans GALaC a été réalisée en 2013
pour l'évaluation AERES : [Diaporama AERES 2013]({filename}/documents/lri-slides-2013.pdf).

## Principales thématiques de recherche
