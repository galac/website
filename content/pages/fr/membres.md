Title: Membres de l'équipe GALaC
slug: members
menuTitle: Membres
description: Les membres de l'équipe GALaC.
lang: fr
page_order: 2
template: page-members

![alt="Photo de groupe" class="test"]({filename}/images/members.jpg){: .img-responsive .centered}

<!-- The list of members is generated from the lri website, if the shown informations are out of date, please update your lri account -->

