Title: Members of GALaC
slug: members
menuTitle: Members 
description: The members of the GALaC team.
lang: en
page_order: 2
template: page-members

![Member Group Photo](/images/members.jpg){: .center} 

<!-- The list of members is generated from the lri website, if the shown informations are out of date, please update your lri account -->

