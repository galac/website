Title: GARN,  Game Algorithms for RNa 3D sampling
slug: GARN
parentpage: softwares
menutitle: GARN
lang: en
description: Game Algorithms for RNa 3D sampling
page_order: 1
members: Mélanie Boudard, Julie Bernauer, Dominique Barth, Johanne Cohen, Alain Denise

[GARN](http://garn.lri.fr) is a software  to compute a sampling of RNA 3D structure from a secondary structure.
GARN can thus be a suitable starting point for large RNA molecular modelling and experimental structure resolution.

Reference

[GARN: Sampling RNA 3D Structure Space with Game Theory and Knowledge-Based Scoring Strategies](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0136444) Boudard M, Bernauer J, Barth D, Cohen J, Denise A (2015) GARN: Sampling RNA 3D Structure Space with Game Theory and Knowledge-Based Scoring Strategies. PLoS ONE 10(8): e0136444. doi: 10.1371/journal.pone.0136444x

## GALaC members involved in GARN

<!-- the list of members is taken from the members parameter -->
