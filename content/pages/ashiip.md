Title: aSHIIP, a generator of random topologies with hierarchy
slug: ashiip
parentpage: softwares
menutitle: aSHIIP
lang: en
description: aShiip is a generator of random topologies with hierarchy.
page_order: 1
members: Marc-Antoine Weisser, Joanna Tomasik

[aSHIIP](http://wwwdi.supelec.fr/software/ashiip) is a generator of random topologies with hierarchy..  It allows one to generate inter-domains networks with the use of various generation models. Generated topologies can vary and reach a size greater than the size of the real-life Internet. Moreover, it can introduce in generated topologies the realistic hierarchy imposed by commercial relationships existing between domains of the Internet.

## GALaC members involved in aSHIIP

<!-- the list of members is taken from the members parameter -->
