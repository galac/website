Title: GALaC team at LRI, Paris-Sud
TitleSEO: GALaC team at LRI, Orsay University Paris-Sud
slug:home
menuTitle: Home
description: GALaC is a research group at LRI, Paris-Sud University. We are focused on grah theory, combinatorics and network distributed systems algorithmic. 
lang: en
page_order: 1
URL:
save_as: index.html
include_category: all

GALaC  is a research group at [LRI](http://www.lri.fr), Paris-Sud University. We are focused on graph 
theory, combinatorics and network distributed systems algorithmic.



A  global presentation of research activities in GALaC was made in 2013 for the AERES
evaluation: [Slides AERES 2013]({filename}/documents/lri-slides-2013.pdf) and [projet]({filename}/documents/LRIreport-GALAC.pdf).


## Recent Posts

<!-- print the list of blog post because of the "include_category" parameter -->
<!-- Add a "number_articles" variable to change the number of posts printed (default is 3) -->


