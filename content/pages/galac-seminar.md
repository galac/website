Title: GALaC team seminar
slug: team-seminar
description: team seminar of GALaC
lang: en
page_order: 1
include_tag: Team seminar
parentpage: seminars

The GALaC team seminar is organized on a regular basis on Friday at 14:00 in the PCRI building (650) at LISN. 


 

## Recent and up-coming seminars

<!-- the list of seminars come from the seminar blog posts -->
<!-- Add a "number_articles" variable to change the number of seminars printed (default is 3) -->
