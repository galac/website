Title: Research
slug: research
description: Research activities at GALaC : Graphs, algorithmic and combinatorics
lang: en
page_order: 3


The GALaC team has three main research areas.

![Graphs Algorithmic and Combinatorics](/images/GalacEnImageTer2016_small.png){ .center .img-responsive}


A  global presentation of research activities in GALaC was made in 2013 for the AERES
evaluation: [Slides AERES 2013]({filename}/documents/lri-slides-2013.pdf) and [projet]({filename}/documents/LRIreport-GALAC.pdf).


## Main research areas

