Title: Algorithms for Networked Systems
slug: network
description: Research activities of the GALaC team in Algorithms and networked systems.
lang: en
page_order: 1
parentpage: research
include_tag: network
thumb_img: and-thumb.png
img: and.png

The research goal of the ANS group is to design efficient modeling, control and performance optimization
algorithms especially tailored for networked and distributed systems, as well as their applications. The
scientific contributions we expect are therefore both theoretical, with the development of new mathematical
modeling techniques and proofs, and applied, with the development of innovative tools for the
optimal planning and resource allocation in cognitive, opportunistic wireless and content-centric networks.
Specifically, the objectives of the ANS group for the next five years are:

- To establish theoretical building blocks for the design and optimization of networked systems, including:
Control Theory, Game Theory, Distributed Algorithms (Self-stabilization, Fault Tolerance), Discrete Event
Simulation systems.
- To design novel, efficient algorithms and protocols based on the developed theoretical framework,
and evaluate their performance in practical networking scenarios. This includes: Opportunistic Wireless
systems (including, among others, Cognitive Radio, Sensor and Robot networks), Future Internet infrastructures
and protocols (Information, Content-Centric Networks), Security in Cyber-physical systems.
More specifically, the challenges we will address are related to dynamic spectrum aggregation, which
has just emerged in the cognitive radio network field, withmany important theoretical and technical problems
waiting to be solved. In this context, we plan to (1) develop a theoretical framework characterizing
the tradeoff between spectrum and energy efficiencies in spectrum aggregation, and (2) design green
dynamic spectrum aggregation mechanisms that jointly optimize the spectrum sensing, spectrum aggregation
and access so as to achieve a desired balance from both spectrum and energy perspectives. This
research work will be conducted in the framework of an ongoing research project, the Green-Dyspan
project (ANR Blanc International II, March 2013-Sep. 2016), in cooperation with Zhejiang University.
Another interesting research field is related to content-centric networks (CCNs), which constitute one of
the most promising paradigms for the future Internet. In this field, several research issues are still open, and
we plan to (1) devise efficient algorithms to foster resource sharing between network nodes, (2) design
optimal resource allocation schemes to plan and manage efficient CCNs in a time-varying context, (3)
game theoretical models to address security/privacy issues arising in mobile CCNs, as a basic building
block to further support cooperative behaviors. This work will be conducted within the framework of the
IUF 5-year project (2013-2018).
We will also study self-stabilization and fault-tolerance properties of different networked systems, like mobile
robot networks, wireless inter-networked systems and sensor networks. Common criteria of those systems
are the hardness of formal analysis due to huge non-determinism, and likeliness of fault occurrence.
Unlike classical fault-tolerance techniques that rely on redundancy and replication (and are hence costly
both in terms of memory, computation, and communication usage), self-stabilization is an optimistic way
to recover from system faults, and is likely to enable lightweight solutions that are suited to those constrained
emerging networks.
