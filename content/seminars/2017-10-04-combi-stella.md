Title: Polytopal Realizations of Finite Type g-Vector Fans
date: 2017-10-04 11:00
slug: 2017-10-04-combi-seminaire
Authors: Salvatore Stella
lang:en
authorurl: http://www1.mat.uniroma1.it/people/stella/index.shtml
institution: University of Haifa, Israel
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

To any cluster algebra with principal coefficients A, and by extension to any other cluster algebra, it is associated a simplicial fan that encodes the underlying combinatorial structure: the g-vector fan. When A is of finite type, this fan has a natural interpretation in terms of a (not necessarily finite) root system. Moreover it is complete, leading to wonder whether it is the normal fan to a polytope or not. A positive answer to this question, whenever the initial seed of A is acyclic, was obtained by Hohlweg, Lange, and Thomas deleting inequalities in the facet description of the associated permutahedron.

We extend the politopality result to all the other possible initial seeds.  Our construction though, while producing the same generalized associahedra in the acyclic cases, does not preserve the connection with Coxeter combinatorics beyond type A_n.

This seminar is based on a joint work with C. Hohlweg and V. Pilaud.
