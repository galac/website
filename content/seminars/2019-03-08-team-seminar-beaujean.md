Title: Fighting epidemics with the maximum spectral subgraph
date: 2019-03-08 14:30 
slug: 2019-03-08-Team-seminar 
Authors: Paul Beaujean
lang:fr
institution: GALAC, LRI
tags: Team seminar, graphs 
location: LRI, 445

**Summary:** Recent developments in mathematical epidemiology have identified a relationship between the time to extinction of an epidemic spreading over a network and the spectral radius of the underlying graph i.e. the largest eigenvalue of its adjacency matrix. At the same time, new generation networking technologies such as NFV (network function virtualization) and SDN (software-defined networking) have enabled real-time reconfiguration of network topology.
In this context, we introduce the maximum spectral subgraph problem: given an undirected graph and a threshold value, the task is to find a partial subgraph with the maximum number of edges such that its spectral radius is bounded above by the threshold. Solving this problem would prescribe an inexpensive network topology reconfiguration which guarantees that a malware epidemic would disappear in a short amount of time.
Unfortunately, the maximum spectral subgraph problem is NP-hard which motivates us to study whether it can be approximated in polynomial time. In this talk, we present a simple randomized (log^2 n)-approximation algorithm based on the relaxation and rounding framework using semidefinite programming together with matrix concentration inequalities.
 
This talk covers joint work with Cristina Bazgan and Eric Gourdin from the paper:
C. Bazgan, P. Beaujean, and E. Gourdin, Relaxation and matrix randomized rounding for the maximum spectral subgraph problem, COCOA 2018 
