Title: Généralisation des polynômes de Symanzik en dimensions supérieures
date: 2019-04-24 11:00
slug: 2019-04-24-combi-poincare-seminaire
Authors: Matthieu Piquerez
lang:fr
institution: CMLS, Ecole Polytechnique
tags: Combi seminar, combinatorics
location: Salle Henri Poincaré du LIX

Les deux polynômes de Symanzik sont des invariants de graphe utilisés en théorie quantique des champs pour calculer des intégrales de Feynman. Le premier polynôme de Symanzik est le dual du polynôme de Kirchhoff pondéré, qui compte le nombre pondéré d'arbres couvrants d'un graphe. En 2009, Duval, Klivans et Martin ont généralisé la notion d'arbre et le théorème de Kirchhoff en dimensions supérieures (sur les complexes simpliciaux). Nous pouvons de même généraliser les polynômes de Symanzik. Les deux polynômes duaux partagent de nombreuses propriétés remarquables, notamment une formule de délétion-contraction et une forme déterminantale. Toutefois, nous verrons que la propriété de stabilité par subdivision, propre aux polynômes de Symanzik, rend ces polynômes peut-être plus intéressants que leurs duaux. [arXiv:1901.09797]
