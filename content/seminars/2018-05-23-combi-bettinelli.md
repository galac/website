Title: Convergence of uniform noncrossing partitions toward the Brownian triangulation
date: 2018-05-23 11:00
slug: 2018-05-23-combi-seminaire
Authors: Jérémie Bettinelli
lang:fr
institution: LIX, équipe Combi
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

We give a short proof that a uniform noncrossing partition of the regular $n$-gon weakly converges toward Aldous's Brownian triangulation of the disk, in the sense of the Hausdorff topology. This result was first obtained by Curien and Kortchemski, using a more complicated encoding. Thanks to a result of Marchal on strong convergence of Dyck paths toward the Brownian excursion, we furthermore give an algorithm that allows to recursively construct a sequence of uniform noncrossing partitions for which the previous convergence holds almost surely.
In addition, we also treat the case of uniform noncrossing pair partitions of even-sided polygons.
