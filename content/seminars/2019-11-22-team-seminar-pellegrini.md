Title: Optimal curing policy for epidemic spreading over a community network with heterogeneous population
date: 2019-11-22 14:30
slug: 2019-11-22-team-seminar
Authors: Francesco De Pellegrini
lang: en
institution: University of Avignon
tags: Team seminar, networks
location: LRI, 445

summary: The design of an efficient curing policy, able to stem an epidemic process at an affordable cost, has to
account for the structure of the population contact network supporting the contagious process. Thus,
we tackle the problem of allocating recovery resources among the population, at the lowest cost possible
to prevent the epidemic from persisting indefinitely in the network. Specifically, we analyze a
susceptible–infected–susceptible epidemic process spreading over a weighted graph, by means of a firstorder
mean-field approximation. First, we describe the influence of the contact network on the dynamics
of the epidemics among a heterogeneous population, that is possibly divided into communities. For the
case of a community network, our investigation relies on the graph-theoretical notion of equitable partition;
we show that the epidemic threshold, a key measure of the network robustness against epidemic
spreading, can be determined using a lower-dimensional dynamical system. Exploiting the computation
of the epidemic threshold, we determine a cost-optimal curing policy by solving a convex minimization
problem, which possesses a reduced dimension in the case of a community network. Lastly, we consider
a two-level optimal curing problem, for which an algorithm is designed with a polynomial time complexity
in the network size.

