Title: Hiérarchies KP/2-Toda et cartes biparties
date: 2019-05-29 11:00
slug: 2019-05-29-combi-seminaire
Authors: Baptiste Louf
lang:fr
institution: IRIF, Univ. Paris 7
tags: Combi seminar, combinatorics
location: Salle Henri Poincaré du LIX

Les hiérarchies intégrables (des ensembles infinis d’EDPs en une infinité de variables) sont étudiées depuis longtemps en physique mathématique. De manière assez surprenante, la série génératrice des cartes est une solution des hiérarchies KP et 2-Toda (qui est une généralistation de la précédente), ce qui permet d’obtenir des formules de récurrences exactes et très simples sur les coefficients (Goulden—Jackson, Carrell—Chapuy, Kazarian—Zograf).
Je décrirai un peu la théorie derrière la hiérarchie 2-Toda et la preuve que la série des cartes en est solution (qui implique entre autres, des fonctions symétriques), et je dériverai une formule permettant de compter les cartes biparties à degrés des faces prescrits.
