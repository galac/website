Title: Generalized Jucys-Murphy elements and canonical idempotents in towers of algebras 
date: 2019-01-09 11:00
slug: 2019-01-09-combi-seminaire
Authors: Aaron Lauve
lang:en
institution: Loyola Univ., Chicago
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

The collection of symmetric group algebras serves as a motivating example for what I'll call a multiplicity-free tower of finite dimensional algebras. Any such family has a canonical complete set of pairwise orthogonal primitive idempotents stemming from its representation theory. In the case of the symmetric group algebras, these idempotents were first given a tidy formula by Thrall (1941) using the infamous "Young symmetrizers." I'll provide other examples (Brauer algebras will feature prominently), then highlight several methods to compute these idempotents, extending work of Jucys, Murphy, and Vershik--Okunkov. Opportunities for SageMath projects will be littered throughout. (Based on work with S. Doty and G.H. Seelinger, arXiv:1606.08900.)

