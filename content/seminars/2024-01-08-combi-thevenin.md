Title: Grands systèmes méandriques et nouille infinie 
date: 2024-01-08 11:00
slug: 2024-01-08-combi-seminar
Authors: Paul Thévenin
lang:fr
institution: University of Vienna
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Poincaré (1912) définit les méandres comme configurations topologiques obtenues à partir de deux courbes fermées simples sur la sphère ayant un nombre fixé de points d'intersection.
Ces objets ont été très étudiés depuis, mais la question principale - leur énumération asymptotique - reste ouverte.
Je considérerai ici des systèmes méandriques, qui ressemblent aux méandres mais sont plus simples d'un point de vue combinatoire.
Un système méandrique est une configuration de boucles non-croisées dans le plan, qui coupent l'axe horizontal en un nombre fixé de points.
J'évoquerai des résultats récents, concernant en particulier le nombre de boucles dans un grand système méandrique uniforme.
Le comportement de cette quantité est lié à la limite locale de ces systèmes méandriques, appelée nouille infinie.
Si le temps le permet, j'aborderai les liens entre systèmes méandriques et partitions non-croisées, ainsi qu'avec un modèle d'arbres décorés. 