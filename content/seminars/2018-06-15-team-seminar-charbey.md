Title: Caractérisation de réseaux égocentrés par l'énumération de leurs sous-graphes induits
date: 2018-06-15 14:30 
slug: 2018-06-15-Team-seminar 
Authors: Raphaël Charbey
lang:fr
institution: GALAC, LRI
tags: Team seminar, graphs 
location: LRI, 455

**Résumé :** La science des réseaux regroupe des méthodes issues de différentes disciplines qui ont néanmoins souvent du mal à percer au delà de celles-ci. Très utilisée en biologie moléculaire, notamment dans le cadre de l'étude des interactions entre protéines, l'énumération de l'ensemble des sous-graphes induits, jusqu'à une taille limite (généralement de 5 sommets, parfois moins), qu'on appelle graphlets, se retrouve ainsi rarement utilisée dans des travaux de sociologie des réseaux. Cette présentation consiste en la description de l'application de cette méthode de caractérisation de réseaux, issus ici de Facebook et plus précisément de relations égocentrés, c'est-à-dire des réseaux formés par les liens entre les amis d'un individu donné, via la participation volontaire de milliers d'utilisateurs à une enquête menée en ligne.
On propose une métrique originale, la représentativité des graphlets, qui nous permet de produire un clustering des graphlets, mettant en évidence des relations structurales entre eux mais également quelques particularités liées à la spécificité des réseaux sociaux. On construit également un clustering de ces réseaux selon la représentativité des graphlets, les regroupant selon leur "forme", en quelque sorte, et indépendamment de leur taille. On discutera également de la taille des graphlets à considérer, sachant que leur énumération est extrêmement gourmande en temps de calcul.



