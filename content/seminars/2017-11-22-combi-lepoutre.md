Title: A bijective proof of the enumeration of maps in higher genus
date: 2017-11-22 11:00
slug: 2017-11-22-combi-seminaire
Authors: Mathias Lepoutre
lang:en
authorurl: http://www.lix.polytechnique.fr/Labo/Mathias.Lepoutre/
institution: LIX, Ecole Polytechnique
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Bender and Canfield proved in 1991 that the generating series of maps in higher genus is a rational function of the generating series of planar maps. In this talk, I will give the first bijective proof of this result. Our approach starts with the introduction of a canonical orientation that enables us to construct a bijection between 4-valent bicolorable maps and a family of unicellular blossoming maps.

