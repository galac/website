Title: Overcoming interference in the beeping communication model
date: 2019-10-11 14:30
slug: 2019-10-11-team-seminar
Authors: Fabien Dufoulon
lang:en
institution: University Paris South
tags: Team seminar, graphs
location: LRI, 445

summary: Small inexpensive inter-communicating electronic devices have become widely available. Although the individual device has severely limited capabilities (e.g., basic communication, constant-size memory or limited mobility), multitudes of such weak devices communicating together are able to form low-cost, easily deployable, yet highly performant networks. Such distributed systems present significant challenges however when it comes to the design of efficient, scalable and simple algorithms.
In this work, we are interested in studying such systems composed of devices with severely limited communication capabilities - using only simple bursts of energy. These distributed systems may be modeled using the beeping model, in which nodes communicate by beeping or listening to their neighbors (according to some undirected communication graph). Simultaneous communications (i.e., collisions) result in non-destructive interference: a node with two or more neighbors beeping simultaneously detects a beep.
Its simple, general and energy efficient communication mechanism makes the beeping model widely applicable. However, that simplicity comes at a cost. Due to the poor expressiveness of beeps and the interference caused by simultaneous communications, algorithm design is challenging. Throughout this work, we overcome both difficulties in order to provide efficient communication primitives. A particular focus is on deterministic and time-efficient solutions independent of the communication graph’s parameters (i.e., uniform).

