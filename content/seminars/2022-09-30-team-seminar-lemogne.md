Title: (q,t)-symmetry in triangular partitions
date: 2022-09-30 14:00
slug: 2022-00-30-team-seminar
Authors: Loïc Le Mogne
lang:fr
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: The study of Dyck paths and parking functions combinatorics is a central piece
of the Diagonal Harmonic Polynomials theory. It is the origin of many currents
problems of algebraic combinatorics. Interactioncs between Dyck paths, parking
functions, the Tamari lattice, symmetric functions and other fields of mathematics
or physics have emerged in recent years. Two fundamental statistics on Dyck
paths, the area under a path and the number of diagonal inversions (also known
as d-inv) play a key part in these interactions.
<br>
Historically, many generalization of Dyck paths have been studied. The first
generalization, a very natural one, has been to study partition of the shape
(mk, m(k − 1), ..., m), leading to what is know as the Fuss-Catalan numbers.
Then the study of partitions under a line from (0, m) to (n, 0) with m and n
co-prime integers led to the Rational Catalan combinatorics. Afterward, the
co-primality was dropped, leading to the Rectangular Catalan combinatorics
Finally, the last generalization, and the one we will place ourselves in, is the
study of the partition lying under a line from (0, s) to (r, 0), r and s being two
positive real numbers.