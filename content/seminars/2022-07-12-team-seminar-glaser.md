Title: Designing truthful mecanism
date: 2022-07-12 11:00
slug: 2022-07-12-team-seminar
Authors: Victor Glaser
lang:en
institution: LISN, Galac
tags: Team seminar, graphs
location: LRI, 465

summary: In this presentation, we will focus on the generalization of knapsack budgeting.
Given a set of projects and a budget, each voter selects a subset of projects; we want to maximize social welfare.
Different measures can describe this (maximizing the minimum utility of the players, maximizing the sum of their utilities, etc).
<br>
We want to design truthful mechanisms (each voter has an incentive to tell the truth).
As this notion contains many impossibility theorems, we limit our study to different ballots
(preference ranking, selection of one or more projects, etc.) and their computational properties.
<br>
The first part will deal with unitary voting (voters choose only one project).
The second part is devoted to ongoing work on binary voting (voters can approve several projects).
<br>
This work is a join work with Johanne Cohen and Valentin Dardilhac.