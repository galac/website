Title: Algèbres tridendriformes, arbres de Schröder et algèbre de Hopf
date: 2024-04-29 11:00
slug: 2024-04-29-combi-seminar
Authors: Pierre Catoire
lang:fr
institution: Univ. du Littoral
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Les concepts d’algèbres dendriformes, respectivement tridendriformes décrivent l’action de certains éléments du groupe symétrique appelés les battages et respectivement les battages contractants sur l’ensemble des mots dont les lettres sont des éléments d’un alphabet, respectivement d'un monoïde. Un lien entre les algèbres dendriformes et tridendriformes sera donné.

Ces algèbres de mots satisfont certains axiomes mais elles ne sont pas dites libres. Cela signifie
qu’elles vérifient des propriétés supplémentaires comme la commutativité. Dans cet exposé, nous
allons décrire l’algèbre tridendriforme libre. Cette dernière sera décrite par des arbres planaires (pas forcément binaires), dits arbres de Schröder. Nous décrirons la structure d’algèbre tridendriforme sur ces arbres de manière non-récursive avant de construire un coproduit sur celle-ci qui en fera une bigèbre dite (3, 2)−dendriforme graduée par le nombre de feuilles.

Une fois ceci établi, nous étudierons cette algèbre de Hopf : dualité, quotients, dimensions,
étude des primitifs...
