Title: Recherche Opérationnelle à Google
date: 2020-03-12 14:30
slug: 2020-02-12-plateau-seminar
Authors: Laurent Perron
lang: fr
institution: Google
tags: Plateau seminar
location: LRI, 445

summary: 
A travers une série d'exemples, je décrierai les applications de la recherche opérationnelle et de l'optimisation discrète à Google.
Puis je me focaliserai sur l'apport récent des techniques des moteurs SAT (satisfiabilité) et les challenges et opportunités qui lui sont liées.
