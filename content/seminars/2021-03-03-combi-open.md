Title: Séminaire ouvert
date: 2021-03-03 10:30
slug: 2020-03-03-combi-seminaire
Authors: Toute l'équipe
lang:fr
institution: LIX, GALAC et IRIF
tags: Combi seminar, combinatorics
location: https://bbb.lri.fr/b/jer-k22-eqk

Lors d'un séminaire ouvert, le thème n'est pas décidé à l'avance. Tous les membres du séminaires sont invités à participer et peuvent proposer le jour même des interventions plus ou moins longues, des démos ou des questions ouvertes au reste de l'équipe.


