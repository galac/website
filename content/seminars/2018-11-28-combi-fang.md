Title: La transformation zeta steep-bounce dans le Cataland parabolique 
date: 2018-11-28 11:00
slug: 2018-11-28-combi-seminaire
Authors: Wenjie Fang
lang:fr
institution: TU Graz
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Etant un objet classique, le treillis de Tamari a beaucoup de généralisations, y compris les treillis $\nu$-Tamari et les treillis de Tamari paraboliques. Dans cet article, ces deux treillis sont unifiés de manière bijective. D'abord nous prouvons que les treillis de Tamari paraboliques sont isomorphes aux treillis de $\nu$-Tamari avec $\nu$ des chemins de rebond. Puis nous introduisons un nouvel objet dit ``arbre colorié à gauche'', et montrons qu'il donne un pont bijectif entre divers objets de Catalan paraboliques et certaines paires emboîtées de chemins de Dyck. En conséquence, nous prouvons la conjecture de steep-bounce par une généralisation de la fameuse transformation zeta dans la combinatoire de $q,t$-Catalan. C'est une collaboration en cours avec Cesar Ceballos et Henri Mühle. 
