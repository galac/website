Title: Language-theoretic methods in semigroup theory
date: 2023-03-13 11:00
slug: 2023-03-13-combi-seminar
Authors: Carl-Fredrik Nyberg Brodda (Université Gustave Eiffel)
lang:fr
institution: LIX
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Language-theoretic methods in combinatorial group theory go back to the fundamental work by Anisimov in the 1970s. Since then, the area has exploded, including such deep theorems as the Muller-Schupp theorem: a group has context-free word problem if and only if it is virtually free. In this talk, I will give a brief overview of historic developments, and then discuss how this area has a rich parallel in the theory of combinatorial semigroup theory. I will give a taste of the kinds of results that can be proved when using language-theoretic methods in semigroup theory, including results on free products, hyperbolic semigroups, analogues of the Muller-Schupp theorem, and how context-free semigroups can behave very differently from context-free groups. I will also discuss how language-theoretic methods for semigroups can shed light on open questions in group theory, including groups admitting an ET0L/indexed multiplication table, or groups with ET0L/indexed word problem.
