Title: Realizing Geometrically s-Permutahedra via Flow Polytopes
date: 2022-12-02 14:00
slug: 2022-12-02-team-seminar
Authors: Daniel Tamayo-Jimenez
lang:en
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: In 2020, Ceballos and Pons defined s-decreasing trees with s
being a weak composition. They described an order on these objects
called the s-weak order which gives them the order structure of a
lattice. They further conjectured that this structure could be realized
geometrically as the 1-skeleton of a polyhedral subdivision of a certain
polytope. In this talk I will present 2 such realizations with the help
of flow polytopes that come from directed graphs. The first uses
triangulations of said polytopes while the second consists of mixed
subdivisions of hypercubes. <br>
This is joint work with Rafael S. González D’León, Alejandro H. Morales,
 Eva Philippe, Yannic Vargas, and Martha Yip.