Title: Tiling Groups: Emptiness and Aperiodicity
date: 2022-05-13 14:00
slug: 2022-05-13-team-seminar
Authors: Nicolas Bitar
lang:fr
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: Tilings of the plane or infinite grid have been a rich field of study for many years.
Through tiling with local rules, it is even possible to create aperiodicity and embed computation in the plane.
But what happens when we begin changing the underlying structure?
We will explore how different underlying graphs and their geometries influence what we can obtain through the use
of local rules and take a look at the state of the art of a particular class of graphs, those coming from infinite groups.
