Title: Taux de croissance des monoïdes de tresses à nombre arbitraire de générateurs
date: 2020-04-15 10:30
slug: 2020-04-15-combi-seminaire
Authors: Vincent Jugé
lang:fr
institution: LIGM, Université Paris Est - Marne-la-Vallée
tags: Combi seminar, combinatorics
location: à distance

En ligne

Soit M un monoïde, muni d'une famille génératrice finie. La question de la croissance du monoïde est la suivante : pour un entier k fixé, combien d'éléments du monoïde peut-on écrire comme produit de k générateurs ? En pratique, si on note m_k cette quantité, la suite m_k^{1/k} admet toujours une limite, que l'on appelle le taux de croissance du monoïde. Dans cette présentation, nous nous intéresserons aux taux de croissance des monoïdes de tresses à n générateurs, et à la limite de ces taux de croissance quand n devient arbitrairement grand. En utilisant des outils issus de la combinatoire algébrique et de l'analyse complexe, nous montrerons en particulier comment calculer cette limite, comment l'interpréter en termes combinatoires, et nous étendrons notre étude au cas de monoïdes apparentés aux monoïdes de tresses, qui sont les monoïdes d'Artin-Tits de type B et D.
