Title: Periodic Pólya urns and asymptotics of Young tableaux
date: 2020-06-10 10:30
slug: 2020-06-10-combi-seminaire
Authors: Michael Wallner
lang:en
institution: LaBRI, Université de Bordeaux
tags: Combi seminar, combinatorics
location: à distance

Pólya urns are urns where at each unit of time a ball is drawn uniformly at random and is replaced by some other balls according to its colour. We introduce a more general model: The replacement rule depends on the colour of the drawn ball AND the value of the time mod p. 

Our key tool are generating functions, which encode all possible urn compositions after a certain number of steps. The evolution of the urn is then translated into a system of differential equations and we prove that the moment generating functions are D-finite in one variable. From these we derive asymptotic forms of the moments. When the time goes to infinity, we show that these periodic Pólya urns follow a rich variety of behaviours: their asymptotic fluctuations are described by a family of distributions, the generalized Gamma distributions, which can also be seen as powers of Gamma distributions.
 
Furthermore, we establish some enumerative links with other combinatorial objects yielding a new result on the asymptotics of Young tableaux: We prove that the law of the lower right corner in a triangular Young tableau follows asymptotically a product of generalized Gamma distributions.

This is joint work with Cyril Banderier and Philippe Marchal.
