Title: Cyclic sieving for reduced reflection factorizations of the Coxeter element 
date: 2018-06-20 11:00
slug: 2018-06-20-combi-seminaire
Authors: Theo Douvropoulos
lang:en
authorurl: http://www-users.math.umn.edu/~douvr001/
institution: IRIF, Paris 7 Diderot
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Given a factorization $t_1\cdots t_n=c$ of some element $c$ in a group, there are various natural cyclic operations we can apply on it; one of them is given by 
$\Psi:(t_1,\cdots,t_n)\rightarrow (c\ t_n\ c^{-1},t_1,\cdots, t_{n-1})$. 
A common question is then to enumerate factorizations with a certain degree of symmetry, that is, those for which $\Psi^k$ is the identity, for a given $k$.
In this talk we present the case of minimal length reflection factorizations of the Coxeter element $c$ of a (well-generated, complex) reflection group $W$. The answer is a satisfying cyclic-sieving phenomenon; namely the enumeration is given by evaluating the following polynomial (in $q$) on a $(nh/k)^{\text{th}}$ root of unity: 
$X(q):=\prod_{i=1}^n\dfrac{[hi]_q}{[d_i]_q},\quad\quad\text{ where }[n]_q:=\dfrac{1-q^n}{1-q}$. 
Here, $h$ is the Coxeter number (the order of $c$) and the $d_i$'s are the fundamental invariants of $W$. The proof is based on David Bessis' pioneering work, which gave a geometric interpretation for such factorizations. We will only sketch the necessary background material and focus more on a topological braid-theoretic representation of cyclic operations such as $\Psi$. This is, in fact, the (combinatorial) heart of the argument.
