Title: Root system chip firing
date: 2017-11-08 11:00
slug: 2017-11-08-combi-seminaire
Authors: Thomas McConville
lang:en
authorurl: http://math.mit.edu/~thomasmc/
institution: MIT
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

The chip firing game is a simple example of a confluent system, which is a central notion in algebraic combinatorics and particle physics. That is, given an initial distribution of chips on a graph, there are many ways to "play" the chip firing game, but they all lead to the same result. Jim Propp introduced a variation on chip firing on the infinite line graph in which the chips are labeled. With this change, the game is no longer confluent from every initial configuration of chips, but it still exhibits some partial confluence. In an attempt to understand this partial confluence, we introduce a generalization called root system chip firing. I will describe these processes and present several conjectures. This is based on joint work with Sam Hopkins, Pavel Galashin, and Alexander Postnikov.
