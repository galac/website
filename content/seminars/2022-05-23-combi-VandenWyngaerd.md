Title: From Delta to Theta conjectures: a survey
date: 2022-05-23 15:00
slug: 2022-05-23-combi-seminaire
Authors: Anna Vanden Wyngaerd
lang:fr
institution: IRIF Université Paris Cité
tags: Combi seminar, combinatorics
location: LIX

In 2015, Haglund Remmel and Wilson proposed two conjectural combinatorial interpretations of a certain symmetric function involving a certain Delta operator, which acts diagonally on the MacDonald polynomials. These formulas generalise the shuffle conjecture (Haglund, Haimal, Loehr, Remmel, Ulyanov 2002), now theorem (Carlsson, Mellit 2018). In this talk, we will first discuss some of the motivation and history of these famous problems. Then, we will talk about our new symmetric function operator Theta and their role in the proof of one of the Delta conjectures and the formulation of a possible unified Delta conjecture. (Based on joint works with Michele D'Adderio, Alessandro Iraci, Yvan Le Borgne, Marino Romero)
