Title: Counting linear regions for neural networks verification
date: 2020-11-25 10:30
slug: 2020-11-25-combi-seminaire
Authors: Guillaume Charpiat & Julien Girard-Satabin 
lang:en
institution: Université Paris-Saclay
tags: Combi seminar, combinatorics
location: https://bbb.lri.fr/b/jer-amh-gpd

https://bbb.lri.fr/playback/presentation/2.0/playback.html?meetingId=39bbfe5742a821b16c1d6669a8ef00da2d072c73-1606295770581

Neural networks are becoming an increasingly popular class of programs. However, their relative lack of safety presents an obstacle
to their wide acceptance yet to be tackled. To approximate complex behaviours, neural networks rely on piece-wise linear activation
functions in their control flow. This piece-wise linear behaviour yield regions on the input space where the neural network behaves linearly.
We devised a method exploiting those linear regions to speed up formal verification on neural networks. This method rely on the counting
of linear regions. We present our method and an algorithm to enumerate the linear regions of a neural network.


