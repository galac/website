Title: Asymptotic behaviour of cyclic automata
date: 2024-02-09 14:00
slug: 2024-02-09-team-seminar
Authors: Benjamin Hellouin de Menibus
lang:en
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: Cyclic dominance describes models where different states (species, strategies...) are in some
 cyclic prey-predator relationship: for example, rock-paper-scissors. This occurs in many contexts such
 as ecological systems, evolutionary games on graphs, etc. Many models exhibit heteroclinic cycles where
 one state dominates almost the whole space before being replaced by the next state in turn. In spatial
 models, this appears often as large moving clusters that dominate locally before being replaced.<br>
We consider the simplest spatial cyclic dominance model, which is the cyclic cellular automata with 3 states
 with synchronous update. When every state has a different initial density, we prove that the asymptotic 
 probability of each state is the intial density of its prey ("you become what you eat"). Such phenomena 
 and its paradoxical consequences (survival of the weakest) have been observed empirically in more complex
 systems, and we give a formal proof in this simple context using particle systems and random walks.
 I will also discuss ongoing work about what kind of prey-predator graphs can be tackled with our approach.<br>
 [associated preprint](https://arxiv.org/abs/1903.12622) + ongoing work with Yvan le Borgne.