Title: Economics of Age of Information (AoI) Management: Pricing and Competition
date: 2019-05-03 14:30 
slug: 2019-05-03-Team-seminar 
Authors: Lingjie Duan
lang:en
institution: GALAC, LRI
tags: Team seminar, networks
location: LRI, 445


**Summary:** 
Fueled by the rapid development of communication networks and sensors in portable devices, today
many mobile users are invited by content providers to sense and send back real-time useful
information (e.g., traffic observations and sensor data) to keep the freshness of the online platforms’
content updates. However, due to the sampling cost in sensing and transmission, an individual may
not have the incentive to contribute the realtime information to help a platform reduce the age of
information (AoI). Accordingly, we propose dynamic pricing for the platform to offer age-dependent
monetary returns and encourage users to sample information at different rates over time. This
dynamic pricing design problem needs to balance the monetary payments to users and the AoI
evolution over time, and is challenging to solve especially under the incomplete information about
users’ arrivals and their private sampling costs. For analysis tractability, we linearize the nonlinear AoI
evolution in the constrained dynamic programming problem, by approximating the dynamic AoI
reduction as a time-average term and solving the approximate dynamic pricing in closed-form.

This economics problem becomes more interesting by further considering the stage after sampling,
where more than one platform coexists in sharing the content delivery network of limited bandwidth,
and one selfish platform’s update may jam or preempt the other’s under negative network
externalities. We formulate the platforms’ interaction as a non-cooperative Bayesian game and show
that they want to over-sample to reduce their own AoI, causing the price of anarchy (PoA) to be
infinity. To remedy this huge efficiency loss, we propose a non-monetary trigger mechanism of
punishment in a repeated game to enforce the platforms’ cooperation to approach the social
optimum.
