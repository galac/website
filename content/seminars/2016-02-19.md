Title: Highly Resource-limited Distributed Systems
date: 2016-02-19 14:30
slug: 2016-02-19-seminar
Authors: Matthias Függer
lang:en
institution: Max-Planck-Institut
tags: Plateau seminar
location: LIX

In this talk I would like to advocate the study of non-classical
distributed systems whose computing nodes can perform only simple
computations under highly restricted communication mechanisms and memory
capabilities.

I will present two problems, we are currently working on, one from chip
design and one from biology, showing that both systems can be modeled as
link reversal systems -- an algorithmic scheme that was introduced by
Gafni and Bertsekas in 1981 to device routing algorithms.

We finally show how to analyze these link reversal algorithms in terms
of linear dynamical systems.

