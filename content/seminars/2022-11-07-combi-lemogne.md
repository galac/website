Title: (q,t)-symmetry in triangular partitions
date: 2022-11-07 11:00
slug: 2022-11-07-combi-seminar
Authors: Loïc Le Mogne
lang:fr
institution: LISN, Galac
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

We study the $(q,t)$ enumeration of the Triangular Dyck paths, i.e. the sub-partitions of the so-called triangular partitions discussed by Bergeron and Mazin. This is a generalization of the general $(q,t)$ enumeration of Catalan objects. We present new combinatorial notions such as the triangular tableau and the deficit statistic and prove the $q,t$ symmetry and Schur positivity for $2$-partitions.
