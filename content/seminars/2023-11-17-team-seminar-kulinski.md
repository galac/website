Title: Musical juggling: from combinatorial modeling to computer assistance with artistic creation (a thesis in the making)
date: 2023-11-17 14:00
slug: 2023-11-17-team-seminar
Authors: Hoang La
lang:en
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: Musical juggling consists of producing music by the very act of juggling. In this context (stemming from a
collaboration between computer science researchers at LISN and a juggling artist), we refer more specifically to 
juggling with balls that each produce a musical note when caught.
<br>
The aim of this seminar is to present this thesis that delves at the same time into theoretical and practical aspects.
<br>
On the one hand, given a melody, we would like to find how it can be juggled. A mathematical and combinatorial framework
called siteswap was established in the 80s to describe juggling patterns. We’ve expanded it to its musical equivalent,
paving the way for the use of juggling automatas, backtracking algorithms, and reductions to problems such as Exact
Cover or SAT.
<br>
On the other hand, we have to keep in mind the jugglers we are working with, since they are the recipients of the tools
we are developing. Indeed, their needs for practical and artistic juggling patterns lead the constraints of our search. 
Moreover, they are both creators of juggling shows who need a software to customise musical juggling patterns, and performers
who need to learn, memorise and train those patterns, for which we may be able to assist through HCI (Human Computer Interaction).
