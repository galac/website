Title: Current trends in Optimal Stopping Problems and Machine Learning
date: 2024-04-12 14:00
slug: 2024-04-12-team-seminar
Authors: Ini Adinya
lang:en
institution: LISN, Galac
tags: Team seminar, networks
location: LRI, 445

summary: Stochastic optimal stopping problems have a wide range of applications, from finance and economics to neuroscience,
 robotics, and energy management. Many real-world applications involve complex models that have driven the development of 
 sophisticated numerical methods.<br>
Recently, computational methods based on machine learning methods have been developed for solving such complex problems, 
even in high dimensions, beyond what traditional numerical methods can achieve. The neural stochastic differential equation (NSDE)
 has gained attention for modeling stochastic representations with great results in various types of applications.<br>
In this talk, we present an overview of some of these methods and the interaction of machine learning and stochastic control.
 We will also investigate the extension of NSDEs that include jumps with some specifics.
