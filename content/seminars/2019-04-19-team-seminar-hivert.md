Title:  Expérimentations sur le calcul hautes performances en combinatoire énumérative et algébrique.
date: 2019-04-19 14:30 
slug: 2019-04-19-Team-seminar 
Authors: Florent Hivert
lang:fr
institution: GALAC, LRI
tags: Team seminar, combinatorics 
location: LRI, 455

**Summary :** 

In this talk, I will report on several experiments around large scale
enumerations in enumerative and algebraic combinatorics.

I'll describe a methodology used to achieve large speedups in several
enumeration problems. Indeed, in many combinatorial structures (permutations,
partitions, monomials, young tableaux), the data can be encoded as a small
sequence of small integers that can often efficiently be handled by a creative
use of processor vector instructions. Through the challenging example of
numerical monoids, I will then report on how Cilkplus allows for a extremely
fast parallelization of the enumeration. Indeed, we have been able to
enumerate sets with more that 10^15 elements on a single multicore machine.

This work was done in collaboration with Jean Fromentin.

