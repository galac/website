Title: Column-convex {0,1}-matrices, consecutive coordinate polytopes and flow polytopes
date: 2023-06-26 11:00
slug: 2023-06-26-combi-seminar
Authors: Rafael S. González D'León (Loyola univ., Chicago)
lang:en
institution: LIX
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

We study normalized volumes of a family of polytopes associated with column-convex {0,1}-matrices. Such a family is a generalization of the family of consecutive coordinate polytopes, studied by Ayyer, Josuat-Vergès, and Ramassamy, which in turn generalizes a family of polytopes originally proposed by Stanley in EC1. We prove that a polytope associated with a column-convex {0,1}-matrix is integrally equivalent to a certain flow polytope. We use the recently developed machinery in the theory of volumes and lattice point enumeration of flow polytopes to find various expressions for the volume of these polytopes, providing new proofs and extending results of Ayyer, Josuat-Vergès, and Ramassamy. This is joint work with Chris Hanusa, Alejandro Morales, and Martha Yip.
