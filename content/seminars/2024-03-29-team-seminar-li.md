Title: Transversals in a collection of graphs with degree conditions
date: 2024-03-29 14:00
slug: 2024-03-29-team-seminar
Authors: Luyi Li
lang:en
institution: LISN, Galac
tags: Team seminar, graphs
location: LRI, 445

summary: Let <strong>G</strong>={G_1, G_2, ... , G_m} be a collection of n-vertex graphs on the same vertex set V (a graph system),
 and F be a simple graph with e(F) ≤ m. If there is an injection f: E(F) → \[m\] such that e &isinv E(G_{f(e)}) for each e &isinv E(F),
 then F is a partial transversal of <strong>G</strong>. If moreover e(F)=m, then it is a transversal of <strong>G</strong>.<br>
From another perspective, we can see <strong>G</strong> as an edge-colored multigraph <strong>G̃</strong> with V(<strong>G̃</strong>)=V 
and E(<strong>G̃</strong>) a multiset consisting of E(G_1), ... , E(G_m), and each edge e of G̃ is colored by i if e &isinv E(G_i). 
Since all edges of F belong to distinct graphs of <strong>G</strong>, we also call F a rainbow subgraph of <strong>G</strong>.
 We say that <strong>G</strong> is rainbow vertex-pancyclic if <strong>G</strong> contains a rainbow cycle of length ℓ for each
 ℓ &isinv \[3, n\], and <strong>G</strong> is rainbow vertex-pancyclic if each vertex of V is contained in a rainbow cycle of <strong>G</strong>
 with length ℓ for every integer ℓ &isinv \[3, n\].<br>
In this talk, we study the rainbow pancyclicity of a collection of graphs and vertex-even-pancyclicity of a collection of bipartite graphs.
 Moreover, we study rainbow Hamiltonicity of a collection of digraphs.<br>
This is joint work with Jie Hu, Hao Li, Ping Li, Xueliang Li and Ningyan Xu.