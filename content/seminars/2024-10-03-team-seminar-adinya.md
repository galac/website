Title: Algorithms for Stochastic Analysis Using SageMath
date: 2024-10-03 14:00
slug: 2024-10-03-team-seminar
Authors: Ini Adinya
lang:en
institution: LISN, Galac
tags: Team seminar, networks
location: LRI, 445

summary: This talk explores algorithms for computations in stochastic analysis and their increasing
relevance in modern mathematics. We present algorithms to explicitly solve Stochastic Differential Equations 
via Itô's Lemma, and derive solutions of certain partial differential equations using Feynman–Kac formula. 
Hence, emphasizing the need for developing SageMath packages for stochastic analysis.