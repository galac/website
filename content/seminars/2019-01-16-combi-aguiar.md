Title: Mobius functions for real hyperplane arrangements.
date: 2019-01-16 11:00
slug: 2019-01-16-combi-seminaire
Authors: Marcelo Aguiar
lang:en
institution: Cornell Univ.
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

We discuss the beginnings of a theory of noncommutative
Mobius functions and its connections to the structure of the algebra
of faces of a hyperplane arrangement. It is to be seen as a
generalization of the theory of Mobius functions for lattices,
developed by Rota and his school in the 70's. Joint work with Swapneel
Mahajan.

