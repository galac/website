Title: Polynômes de Jack et constellations b-déformées
date: 2024-01-22 11:00
slug: 2024-01-22-combi-seminar
Authors: Victor Nador
lang:fr
institution: LaBRI, Bordeaux
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

 La série génératrice des cartes orientables pondérées (et sa généralisation aux constellations) peut s’exprimer simplement à l’aide des fonctions de Schur. La série des cartes non-orientées (c’est à dire orientable ou non) admet une expression similaire où les fonctions de Schur sont remplacées par les polynomes zonaux. Dans ces deux cas, il existe une grande diversité d’outil permettant d’étudier ces objets: théorie des représentations, décomposition combinatoires, lien avec la théorie des matrices aléatoires, propriétés d’intégrabilité, etc.. Les polynômes de Jack sont une déformation à un paramètre b (dite b-déformation) qui interpole ces deux cas. Seulement, la déformation rend plus délicate l’étude de la série génératrice associé, source de deux conjectures proposées par Goulden et Jackson en 96.

Dans cet exposé, je rappellerai les propriétés des constellations b-déformées introduites récemment par Chapuy et Dolega et soulignerait certaines des difficultés induite par la b-déformation pour étudier ces objets. Je présenterai des résultats récents en collaboration avec V. Bonzom nous permettant d’extraire un ensemble d’EDP pour certains modèle de constellations b-déformées.