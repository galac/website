Title: Parking sur l’arbre binaire infini
date: 2024-01-15 11:00
slug: 2024-01-15-combi-seminar
Authors: Alice Contat
lang:fr
institution: Université Sorbonne Paris Nord
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Considérons un arbre enraciné dont les sommets seront interprétés comme des places de parking, chaque place pouvant accueillir au maximum une voiture. Sur chaque sommet de l’arbre, on ajoute une étiquette entière et positive représentant le nombre de voitures arrivant sur ce sommet. Chaque voiture essaie de se garer sur son sommet d'arrivée et, si la place est occupée, elle descend en direction de la racine de l'arbre jusqu'à ce qu'elle trouve un sommet vide où se garer. S'il n'y a pas de sommet libre sur le chemin vers la racine, la voiture sort de l'arbre, contribuant ainsi au flux de voitures à la racine. Ce modèle présent une transition de phase intéressante que nous allons analyser en détail. Après un aperçu du cas où l'arbre sous-jacent est un arbre critique de Bienaymé-Galton-Watson, nous nous concentrerons sur le cas del'arbre binaire infini, où la transition de phase qui s'avère être "discontinue". 
L'exposé est basé sur un travail joint avec David Aldous, Nicolas Curien et Olivier Hénard.