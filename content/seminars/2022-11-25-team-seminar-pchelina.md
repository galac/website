Title: Density of sphere packings: from coins to oranges
date: 2022-11-25 14:00
slug: 2022-11-25-team-seminar
Authors: Daria Pchelina
lang:en
institution: LIPN, Université Paris 13
tags: Team seminar, networks
location: LRI, 445

summary: How to stack an infinite number of oranges to maximize the proportion of the covered space?
Kepler conjectured that the "cannonball" packing is an optimal way to do it. This conjecture took almost
400 years to prove and the proof of Hales and Ferguson consists of 6 papers and over 50000 lines of code.
Given an infinite number of coins of 3 fixed radii, how to place them on an infinite table to maximize the
proportion of the covered surface? Triangulated disc packings are those where each "hole" is bounded by 
three tangent discs. Connelly conjectured that when such packings exist, one of them maximizes the proportion
of the covered surface. We proved that this statement holds for 31 triplets of disc radii and disproved it
for 40 other triplets.<br>
In this talk, we will discover main ideas of the proof of the Kepler conjecture through our results on the 
ternary triangulated disc packings.
