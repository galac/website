Title: Maximum Independent Set in H-free graphs
date: 2018-10-05 14:30 
slug: 2018-10-05-Team-seminar 
Authors: Edouard BONNET
lang:fr
institution: GALAC, LRI
tags: Team seminar, graphs 
location: LRI, 455

**Summary:**  Maximum Independent Set (MIS) in graphs without a connected H as an induced subgraph (that is, H-free) is NP-complete when H is not a tree with at most one vertex of degree at least 3. For the other graphs H, which are paths and subdivisions of the claw (K_1,3), the computational complexity of MIS is generally open except for a small number of cases.
We will explore the parameterized complexity of MIS in H-free graphs.
Our distant goal is to establish a dichotomy of the H for which the problem can be solved in time f(k)n^{O(1)} and the ones for which such an algorithm is unlikely to exist (where n is the total number of vertices, k is the size of the solution, and f is any computable function).
Recast in the parameterized complexity language, we want to know when the problem is FPT and when it is W[1]-complete.
We will present a selection of results and draw a parallel with the situation for the 'classical dichotomy' (P/NP-complete).

This is joint work with Nicolas Bousquet, Pierre Charbit, Stéphan Thomassé, and Rémi Watrigant.
