Title: A Closure Lemma for tough graphs and Hamiltonian degree conditions
date: 2023-04-07 14:30
slug: 2023-04-07-team-seminar
Authors: Cléophée Robin
lang:en
institution: Wilfrid Laurier University, Waterloo (Canada)
tags: Team seminar, networks
location: LRI, 445

summary: A graph G is Hamiltonian if there exists a cycle in G containing all vertices of G
 exactly once. A graph G is t-tough if, for all subsets of vertices S, the number of connected
 components in G−S is at most |S|/t. We extended the theorem of Hoàng by proving the following:
 Let G be a graph with degree sequence d_1,d_2,…,d_n and let t be a positive integer at most 4.
 If G is t-tough and if for every integer i in &#91;t,n/2&#93;, d_i ≤ i ⇒ d_{n−i+t} ≥ n−i, then G is Hamiltonian.
 To do this we extend the closure lemma due to Bondy and Chvàtal.