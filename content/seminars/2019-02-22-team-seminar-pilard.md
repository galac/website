Title: Self-Stabilization and Byzantine Tolerance for Maximal Matching
date: 2019-02-22 14:30 
slug: 2019-02-22-Team-seminar 
Authors: Laurence Pilard
lang:fr
institution: GALAC, LRI
tags: Team seminar, networks
location: LRI, 445


**Summary:** We analyse the impact of transient and Byzantine faults on the construction of a maximal matching in a general network. 
In particular, we consider the self-stabilizing algorithm called AnonyMatch presented by Cohen et al. in PPL'2016 for computing such a matching. 
Since self-stabilization is transient fault tolerant, we prove that this algorithm still works under the more difficult context of arbitrary Byzantine faults. 
Byzantine nodes can prevent nodes close to them from taking part in the matching for an arbitrarily long time. 
We give some bound on their impact depending on the distance between a non-Byzantine node and the closest Byzantine, called the containment radius. 
We present the first algorithm tolerating both transient and Byzantine faults under the fair distributed daemon while keeping the best known containment radius. 
