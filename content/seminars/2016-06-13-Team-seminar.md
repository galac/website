Title:Théorie des représentations combinatoires de tours de monoïdes, Application à la catégorification et aux fonctions de parking
date: 2016-06-13 14:00 
slug: 2016-06-13-Team-seminar 
Authors:  Aladin Virmaux
lang:en 
institution: GALAC, LRI
tags: team seminar 
location: LRI, 435, salle des theses


Bonjour,

 J'ai le plaisir de vous inviter à ma soutenance de thèse intitulée

 «Théorie des représentations combinatoires de tours de monoïdes,
 Application à la catégorification et aux fonctions de parking».

Cette thèse se tiendra le lundi 13 juin 2016 à 14h30, à l'Université
Paris-Sud, au bâtiment Claude Shannon (660), Amphithéâtre 40.

Accès:
-https://www.lri.fr/info.pratiques.php
GPS:
-https://www.openstreetmap.org/#map=17/48.71212/2.16647

Le jury sera composé de:

M. Nantel Bergeron, Professeur à York University, Rapporteur
M. Stuart Margolis, Professeur à Bar Ilan University, Rapporteur
M. Alin Bostan, Chargé de Recherche, Examinateur
M. Jean-Christophe Novelli, Professeur de l'Université Marne-la-Vallée, Examinateur
M. Jean-Éric Pin, Directeur de Recherche, Examinateur
Mme. Michèle Sebag, Directrice de Recherche, Examinatrice
M. Nicolas M. Thiéry, Professeur de l’Université Paris-Sud, Directeur de thèse


La soutenance sera suivie d'un pot de thèse auquel vous êtes bien sûr
invités.

Résumé court:
-------------

Cette thèse se situe en combinatoire algébrique et plus
particulièrement en théorie des représentations des tours de monoïdes.
Dans une première partie, nous étudions les liens entre la
combinatoire des représentations de certaines tours de monoïdes et
celle d'algèbres de Hopf associées. Nous montrons que ces liens sont
très rigides et l'illustrons par un résultat de non-existence dans le
cas de l'algèbre de Hopf PBT et un résultat d'unicité de la
catégorification de Krob-Thibon. Dans la deuxième partie, nous
appliquons la théorie des représentations à l'étude des fonctions de
parking afin d'en tirer des formules d'énumération.

Résumé long:
-------------

Cette thèse se situe en combinatoire algébrique, et plus
particulièrement en théorie combinatoire des représentations
linéaires des monoïdes finis.

Rappelons qu'un monoïde est un ensemble fini $M$ muni d'une
multiplication et d'un élément neutre, et qu'une représentation de $M$
est un morphisme de M dans le monoïde des matrices M_n(k) où k est un
corps, typiquement k=C. Les résultats des dernières décennies donnent
un contrôle assez fin sur les représentations des monoïdes, permettant
souvent de se ramener à de la théorie des représentations des groupes
et de la combinatoire sur des préordres.

En 1996, Krob et Thibon ont montré que l'induction et la restriction
des représentations irréductibles et projectives de la tour des
0-algèbres de Hecke H_n(0) permet de munir l'ensemble de ses
caractères d'une structure d'algèbre de Hopf, isomorphe a l'algèbre de
Hopf NCSF des fonctions symétriques non commutatives. Cela donne une
catégorification de NCSF, c'est-à-dire une interprétation de celle-ci
en terme de théorie des représentations.  Ils prolongent ainsi un
résultat dû à Frobenius établissant l'isomorphisme entre l'algèbre de
Hopf des caractères de la tour des groupes symétriques et les
fonctions symétriques.

Un problème naturel depuis lors est d'essayer de catégorifier d'autres
algèbres de Hopf -- par exemple l'algèbre PBT des arbres binaires de
Loday et Ronco -- par des tours d'algèbres. Deviner une telle tour
d'algèbres est difficile en général. Dans le cadre de ce manuscrit on
restreint le champ de recherche aux tours de monoïdes, afin de mieux
contrôler leurs représentations. C'est naturel car ce cadre couvre en
fait les deux exemples fondamentaux ci-dessus, tandis qu'il est
impossible de catégorifier NCSF avec seulement une tour de groupes.

Nous commençons par donner quelques résultats sur les représentations
des tours de monoïdes. Nous nous intéressons ensuite à la
catégorification par des tours de semi-treillis, et en particulier de
quotients du permutoèdre.  Avec ceux-ci, nous catégorifions la
structure de cogèbre de FQSym sur la base G et celle d'algèbre de
FQSym sur la base F. Cela ne permet cependant pas de catégorifier
simultanément toute la structure de Hopf de ces algèbres.

Dans un second temps, nous menons une recherche exhaustive des
catégorifications de PBT. Nous montrons que, sous des hypothèses
naturelles, il n'existe pas de catégorification de PBT par une tour de
monoïdes apériodiques.  Enfin, nous démontrons que, dans un certain
sens, la tour des monoïdes 0-Hecke est la tour de monoïdes la plus
simple catégorifiant NCSF.  La seconde partie porte sur les fonctions
de parking, par application des résultats de la première partie. D'une
part, nous étudions la théorie des représentations de la tour des
fonctions de parking croissantes.

D'autre part, dans un travail commun avec Jean-Baptiste Priez, nous
reprenons une généralisation des fonctions de parking due à Stanley et
Pitman. Afin d'obtenir des formules d'énumérations, nous utilisons une
variante -- plus efficace dans le cas présent -- de la théorie des
espèces. Nous donnons une action de H_n(0) (et non du groupe
symétrique) sur les fonctions de parking généralisées, et utilisons le
théorème de catégorification de Krob et Thibon, pour relever dans les
fonctions symétriques non commutatives le caractère de cette action.

Au plaisir de vous voir,
Aladin Virmaux