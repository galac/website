Title: Calculer le taux de croissance du nombre de motifs d'un pavage
date: 2017-12-22 14:30 
slug: 2017-12-22-Team-seminar 
Authors: Benjamin Hellouin
lang:fr
institution: GALAC, LRI
tags: Team seminar, combinatorics 
location: LRI, 455

**Résumé :** 

Les pavages correspondent à des coloriages d'une grille (Z ou Z^d) qui obéissent à des contraintes locales. Compter ou énumérer les coloriages admissibles de sous-ensembles finis est une question combinatoire naturelle, et le taux de croissance de ce nombre correspond à une notion d'entropie qui apparait en systèmes dynamiques, en physique statistique et en théorie de l'information. Peut-on calculer algorithmiquement ce taux de croissance, étant donné en entrée une description des contraintes locales ?

Pour des contraintes en nombre fini et en dimension 1, l'entropie est calculable par une méthode algébrique classique. Le cas de la dimension supérieure s'est révélé beaucoup plus difficile, et beaucoup d'exemples simples restent ouverts. En 2007, il a été prouvé que l'entropie n'est pas calculable en dimension 2. Des travaux plus récents ont montré que l'entropie redevient calculable sous des hypothèses de mélange, une forme d'indépendance des motifs assez éloignés. Où se situe la limite entre les cas calculable et incalculable ?

En introduisant une notion de taux de mélange, nous montrons l'existence d'un seuil où la difficulté du problème passe de calculable à incalculable, sans pouvoir le déterminer exactement. Pour une famille plus large de pavages soumis à un nombre infini de contraintes, nous parvenons à déterminer exactement la position du seuil, et conjecturons que la situation est la même dans le cas fini.

Ces travaux sont en collaboration avec Silvère Gangloff.

