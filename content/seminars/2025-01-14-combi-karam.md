Title: À la lumière de quelques propriétés essentielles
date: 2025-01-14 13:30
slug: 2024-01-14-combi-seminar
Authors: Thomas Karam
lang:fr
institution: Univ. Oxford
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Cet exposé consistera en quatre parties, chacune commençant par une introduction à un domaine de recherche et terminant par quelques contributions.

Nous débuterons par expliquer comment la conjecture polynomiale de Hales-Jewett à densité unifie plusieurs des généralisations du théorème de van der Waerden, ainsi que comment cette généralisation commune présente une nouvelle classe de difficultés qui est indépendante de celles de toutes les combinaisons d’un sous-ensemble des généralisations.

Ensuite, nous nous attarderons sur les propriétés de base des rangs des tenseurs : malgré le fait qu’une propriété «parfaite» portant sur le rang des matrices ne s’étende souvent pas telle quelle aux rangs des tenseurs, la généralisation à laquelle l’on peut penser en premier lieu peut souvent être rectifiée en une propriété qui simultanément demeure relativement simple et demeure dans l’esprit de la propriété originale.

Nous poursuivrons par les propriétés des restrictions au cube Booléen des polynômes sur les corps finis: là encore, bien des propriétés (notamment le théorème d’équidistribution de Green et Tao de 2007) peuvent être étendues et rectifiées.

Enfin, notre dernier thème principal sera l’adaptation de l’inégalité de Brunn-Minkowski à d’autres espaces ambiants que R^n, comme le tore (R/Z)^n, dans lequel choisir des ensembles A, B unidimensionnels mène à un ensemble somme A+B de volume bien plus petit que la borne dans l’inégalité de Brunn-Minkowski ; en revanche, l’inégalité originale tient-elle tout de même dans (R/Z)^n si l’on compense par une hypothèse sur la dimension des ensembles A, B ?
