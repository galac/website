Title: Théorie des représentations combinatoires de tours de monoïdes, Application à la catégorification et aux fonctions de parking»
date: 2016-06-10 14:30 
slug: 2016-06-10-seminar 
Authors: Aladin Virmaux 
lang:en 
institution: LRI 
tags: Team seminar 
location: LRI, 435, salle des theses

Résumé : Nous étudions tout d'abord les
liens entre la combinatoire des représentations de certaines tours de monoïdes
et certaines algèbres de Hopf. Nous montrons que ces liens sont très rigides et
l'illustrons par un résultat de non-existence dans le cas de l'algèbre de Hopf
PBT et un résultat d'unicité de la catégorification de Krob-Thibon.
Dans un second temps, nous appliquons la théorie des représentations à l'étude
des fonctions de parking afin d'en tirer des formules d'énumération.
