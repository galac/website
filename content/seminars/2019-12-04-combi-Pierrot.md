Title: Formes limites de permutations à motifs interdits
date: 2019-12-04 10:30
slug: 2019-12-04-combi-seminaire
Authors: Adeline Pierrot
lang:en
institution: LRI, Université Paris Saclay
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

On s'intéresse aux ensembles de permutations à motifs exclus, appelés classes de permutations, qui ont été beaucoup étudiés en combinatoire énumérative. Dans ce travail, à la frontière entre combinatoire et probabilités, on s'intéresse à la limite d'échelle d'une grande permutation aléatoire uniforme dans une classe de permutation. On décrit cette limite en terme de permuton, objet pouvant être vu comme une permutation de taille infinie. Un outil clé est la décomposition par substitution des permutations, qui permet de voir les permutations comme des arbres.

