Title: Conjecture d'unistructuralité des algèbres amassées
date: 2018-04-18 11:00
slug: 2018-04-18-combi-seminaire
Authors: Véronique Bazier-Matte
lang:fr
authorurl: http://lacim.uqam.ca/etudiants/
institution: LaCIM, UQAM
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

 En 2014, Assem, Schiffler et Shramchenko ont émis comme conjecture que toute algèbre amassée est unistructurelle, c'est-à-dire que l'ensemble des variables amassées détermine uniquement la structure d'algèbre amassée.  Cette conjecture a été prouvée pour les algèbres de type fini, de rang 2 ou de type A-tilde. Dans cet exposé, nous exposerons quelques définitions et propriétés de base des algèbres amassées et nous expliquerons le concept d'unistructuralité des algèbres amassées. Puis, nous esquisserons la preuve pour certains cas où la conjecture est vérifiée. Pour ce faire, nous utiliserons les triangulations de surface et l'indépendance algébrique des amas.
