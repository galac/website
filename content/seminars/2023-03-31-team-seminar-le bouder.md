Title: Memory-Optimization for Self-Stabilizing Distributed Algorithms
date: 2023-03-31 14:00
slug: 2023-03-31-team-seminar
Authors: Gabriel Le Bouder
lang:fr
institution: LIP6
tags: Team seminar, networks
location: LRI, 445

summary: Self-stabilization is a suitable paradigm for distributed systems,
 particularly prone to transient faults. Errors such as memory or messages corruption,
 break of a communication link, can put the system in an inconsistent state.
 A protocol is self-stabilizing if, whatever the initial state of the system,
 it guarantees that it will return a normal behavior in finite time.
 Several constraints concern algorithms designed for distributed systems.
 Asynchrony is one emblematic example. With the development of networks of connected,
 autonomous devices, it also becomes crucial to design algorithms with a low energy consumption,
 and not requiring much in terms of resources.
<br>
 One way to address these problems is to aim at reducing the size of the messages exchanged between
 the nodes of the network. This thesis focuses on the memory optimization of the communication for
 self-stabilizing distributed algorithms. We will present a negative results, proving the impossibility
 to solve some problems under a certain limit on the size of the exchanged messages, and a particularly
 efficient algorithms in terms of memory for one fundamental problem in distributed systems: the token circulation.

