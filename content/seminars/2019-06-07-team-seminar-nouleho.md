Title: graph algorithms to help molecular construction
date: 2019-06-07 14:30 
slug: 2019-06-07-Team-seminar 
Authors: Stefi Nouleho
lang:fr
institution: GALAC, LRI
tags: Team seminar, graphs 
location: LRI, 445

**Summary:** 
In organic chemistry, when a new molecule is designed, it is necessary to determine chemical reactions that can be used to synthesize this target molecule from available compounds. Finding such chemical reactions consists usually in searching in a reaction database (such as REAXYS or ChEBI) for a molecule that is structurally close to the target molecule. As it is sometimes proposed in various existing approaches, we assume here that two molecules have a similar structure if they have a similar interconnection of elementary cycles (typically carbon cycles) of their molecular graphs. Thus, it is therefore a question of being able to algorithmically select molecules in a reaction database that are structurally similar to a target molecule, by considering representation of molecules by graphs well adapted to structural similarity. We propose a new definition of the graph of cycle of a molecule being more specific to an efficient computation  of structural similarities.
