Title: TracInAD: Measuring Influence for Anomaly Detection
date: 2022-10-21 14:00
slug: 2022-10-21-team-seminar
Authors: Hugo Thimonier
lang:fr (en on demand)
institution: LISN, Galac
tags: Team seminar, networks
location: LRI, 445

summary: As with many other tasks, neural networks prove very effective for anomaly detection purposes. 
However, very few deep-learning models are suited for detecting anomalies on tabular datasets.
This talk proposes a novel methodology to flag anomalies based on TracIn, an influence measure initially
introduced for explicability purposes. The proposed methods can serve to augment any unsupervised deep 
anomaly detection method. We test our approach using Variational Autoencoders and show that the average 
influence of a subsample of training points on a test point can serve as a proxy for abnormality.
Our model proves to be competitive in comparison with state-of-the-art approaches: it achieves comparable
or better performance in terms of detection accuracy on medical and cyber-security tabular benchmark data.