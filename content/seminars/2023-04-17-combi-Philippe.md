Title: Coordonnées explicites pour le s-permutoèdre
date: 2023-04-17 11:00
slug: 2023-04-17-combi-seminar
Authors: Eva Philippe (IMJ)
lang:fr
institution: LIX
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

En 2019, Viviane Pons et Cesar Ceballos ont introduit une généralisation de l'ordre faible avec un paramètre s qui est un vecteur d'entiers. Ils ont conjecturé (et prouvé en dimensions 2 et 3) que cette structure admettait une réalisation géométrique comme complexe polytopal.
En collaboration avec Daniel Tamayo Jiménez, Rafael Gonzalez D'Leon, Alejandro Morales, Martha Yip et Yannic Vargas nous avons récemment prouvé cette conjecture en utilisant une triangulation d'un polytope de flot.
Dans cet exposé j'aimerais rentrer dans le détail du calcul des coordonnées que l'on obtient et demander aux personnes présentes si elles ont des idées de simplification.

