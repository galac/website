Title: Cartes planaires à degrés prescrits : énumération et limites d'échelle
date: 2019-04-17 11:00
slug: 2019-04-17-combi-poincare-seminaire
Authors: Cyril Marzouk
lang:fr
institution: IRIF, Paris 7
tags: Combi seminar, combinatorics
location: Salle Henri Poincaré du LIX

Une carte planaire finie peut se concevoir comme le recollement topologique de polygones qui forme une sphère ; ainsi, étant donné n polygones, on peut considérer l'ensemble (fini) de tels recollement que l'on peut former. À l'aide d'une bijection avec des arbres étiquetés, nous verrons comment énumérer simplement cet ensemble (dans le cas biparti) et nous étudierons le comportement de la structure de graphe d'une telle carte choisie uniformément au hasard lorsque le nombre n de faces tend vers l'infini. En particulier, nous identifierons l'ordre de grandeur du diamètre et des distances typiques et nous verrons des conditions optimales sur les tailles des polygones pour que l'ensemble des sommets muni de la distance de graphe mise à l'échelle converge en loi vers une limite universelle appelée « carte brownienne ».
Travail disponible en ligne : arXiv:1902.04539 & arXiv:1903.06138.
