Title: Introduction à l'apprentissage par renforcement
date: 2022-04-15 14:00
slug: 2022-04-15-team-seminar
Authors: Marc Velay
lang:fr (en on demand)
institution: LISN, Galac
tags: Team seminar, networks
location: LRI, 445

summary: Cet exposé proposera une introduction à l'apprentissage par renfocement, en présentant
 les fonctions à maximiser et les types d'approches employées.
 La présentation suivra l'évolution historique du domaine, du Q-Learning au Deep Reinforcement Learning, 
 en passant par des travaux étant devenus populaires tel que AlphaGo. 
 Nous conclurons sur les enjeux du RL et les travaux d'exploration en cours. 
