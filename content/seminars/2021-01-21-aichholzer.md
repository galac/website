Title:  Crossing Numbers of K_n for Geometric and Topological Drawings
date: 2021-01-21 14:00
slug: 2021-01-21-combi-seminaire
Authors: Oswin Aichholzer
lang:en
institution: TU Graz
tags: Combi seminar, combinatorics
location: https://bbb.lri.fr/b/jer-k22-eqk

https://bbb.lri.fr/playback/presentation/2.0/playback.html?meetingId=02b8d542f7dedeb822e4e461ae6aab733cec4e88-1611233456441

In the area of crossing  numbers we ask for minimizing the number of edge intersections in a drawing of a graph.
There is a rich variety of crossing number problems: Which graphs do we consider, what exactly is a drawing of a
graph and on which surface is it drawn, and how are intersections counted? In this talk we will concentrate on the
crossing number of complete graphs drawn in the plane, including their rich history around Hill's conjecture.

We will have a look at geometric drawings (vertices are points in the plane and edges of the graph are straight line
segments), and simple drawings (edges are simple Jordan arcs with at most one pairwise intersection), which are
also called simple topological graphs. Our results ar based on a representation of the complete graph with order
types (for geometric graphs) and rotation systems (for simple drawings). We will presen recent developements,
as well as (old and new) open questions.
