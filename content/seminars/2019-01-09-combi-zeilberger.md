Title: A proof-theoretic analysis of the rotation lattice of binary trees
date: 2019-01-09 14:00
slug: 2019-01-09-combi-parsifal-seminaire
Authors: Noam Zeilberger
lang:en
institution: Birmingham University
tags: Combi seminar, combinatorics
location: Salle Poincaré du LIX

**Join seminar with the Parsifal team**

The classical Tamari lattice Yn is defined as the set of binary trees
with n internal nodes, with the partial ordering induced by the
(right) rotation operation. It is not obvious why Yn is a lattice, but
this was first proved by Haya Friedman and Dov Tamari in the late
1950s. More recently, Frédéric Chapoton discovered another surprising
fact about the rotation ordering, namely that Yn contains exactly
$2(4n+1)! / ((n+1)!(3n+2)!)$ pairs of related trees. (Even more
surprising was how Chapoton discovered this formula: via the Online
Encyclopedia of Integer Sequences, because the formula had already
been computed in the early 1960s by Bill Tutte, but for a completely
different family of objects!)

In the talk I will describe a new way of looking at the rotation
ordering that is motivated by old ideas in proof theory. This will
lead us to systematic ways of thinking about:

1. the lattice property of Yn, and
2. the Tutte-Chapoton formula for the number of intervals in Yn.

No advanced background in either proof theory or combinatorics will
be assumed.

(Based on the following paper: [https://arxiv.org/abs/1803.10080](https://arxiv.org/abs/1803.10080).)

