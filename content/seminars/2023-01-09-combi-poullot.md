Title: Associaèdres cycliques et degrés intrinsèques des arborescences non-croisées
date: 2023-01-09 11:00
slug: 2023-01-09-combi-seminar
Authors: Germain Poullot
lang:fr
institution: LIX
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Le polytope de pivot d'un polytope P est une généralisation de son polytope des chemins monotones qui vise a capturer le comportement de la "shadow vertex rule" (une règle de pivot importante en optimisation linéaire et dans le domaine des polytopes de fibre). Il a récemment été montré que le polytope de pivot de tout simplexe est un associaèdre. Dans ce contexte, les polytopes cycliques sont de bons candidats pour généraliser le simplexe : après avoir présenté le polytope de pivot en général, nous nous intéresserons au polytope de pivot des polytopes cycliques. Nous verrons que ce sont des projections d'associaèdres que nous appellerons associaèdres cycliques. La bijection des sommets de l'associaèdre vers les arborescences non-croisées (une famille de Catalan) nous indique une bijection entre les sommets des associaèdres cycliques et une sous-famille des arborescences non-croisées. La caractérisation de cette sous-famille, cœur de l'exposé, mènera à la définition d'un degré intrinsèque associé à une arobrescence non-croisée (et par extension à tout objet de Catalan).
