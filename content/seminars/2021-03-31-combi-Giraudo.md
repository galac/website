Title: Some combinatorial aspects of combinatory logic / Quelques aspects combinatoires de la logique combinatoire
date: 2021-03-31 10:30
slug: 2021-03-31-combi-seminaire
Authors: Samuele Giraudo
lang:en
institution: LIGM
tags: Combi seminar, combinatorics
location: https://bbb.lri.fr/b/jer-amh-gpd

https://bbb.lri.fr/playback/presentation/2.0/playback.html?meetingId=39bbfe5742a821b16c1d6669a8ef00da2d072c73-1617178783965

La logique combinatoire est un formalisme introduit par Schönfinkel dans les années 1920
dans le but de supprimer la nécessité des variables dans les expressions. Ce formalisme
donne également lieu à un modèle de calcul très proche du $\lambda$-calcul basé sur la
réécriture de termes et donc sur la réécriture d'arbres. Malgré son nom, ce domaine a été
assez peu étudié d'un point de vue combinatoire. Je propose donc dans cet exposé d'approcher
ainsi quelques systèmes de logique combinatoire. L'exposé commencera par des rappels
généraux sur les systèmes de réécriture de termes. Je vais ensuite parler de logique
combinatoire et lister quelques questions qui me motivent dans ce contexte. Je terminerai
par une première étude (encore en cours) d'un nouveau treillis qui émerge naturellement de
l'un de ces systèmes.

