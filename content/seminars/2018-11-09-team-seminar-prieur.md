Title: De la sociologie avec des algorithmes à la sociologie des algorithmes
date: 2018-11-09 14:30 
slug: 2018-11-09-Team-seminar 
Authors: Christophe Prieur
lang:fr
institution: GALAC, LRI
tags: Team seminar
location: LRI, 455

**Summary:**  
La décennie 2000 a vu l’émergence d’une sociologie parfois dite computationnelle, sociologie qui s’appuie sur des traces collectées numériquement en très grandes quantités, nécessitant des algorithmes spécifiques.  En passant en revue quelques exemples de cette sociologie *avec* des algorithmes, je montrerai comment cet aperçu laisse voir une autre sociologie, qui dans le prolongement de la sociologie des sciences et des techniques, s’intéresse aux algorithmes non comme outils mais comme enjeux dans des processus sociaux dont les acteurs et actrices sont des chercheuses, des ingénieurs, des décideuses politiques, des commerciaux, des techniciennes, des petites mains…
