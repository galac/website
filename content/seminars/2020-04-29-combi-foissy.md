Title: Structures algébriques sur les partitions non croisées
date: 2020-04-29 10:30
slug: 2020-04-29-combi-seminaire
Authors: Loïc Foissy
lang:fr
institution: Université du Littoral
tags: Combi seminar, combinatorics
location: à distance

En ligne

La théorie des probabilités libres utilise la combinatoire des
partitions non croisées pour établir par exemple des relations
entre la famille des moments et la famille des cumulants libres
associée à une variable aléatoire.
Nous allons décrire les opérations algébriques sous-jacentes
(compositions, produits et coproduits) structurant ces relations.

Ces résultats sont issus d'un article coécrit avec Kurusch Ebrahimi-Fard,
Joachim Kock et Frédéric Patras.
