Title: Forecasting multivariate time series with attention mechanism and unsupervised learning
date: 2023-10-13 14:00
slug: 2023-10-13-team-seminar
Authors: Philippe Rambaud
lang:en
institution: LISN, Galac
tags: Team seminar, networks
location: LRI, 445

summary: In the realm of newborn healthcare, identifying neurological pathologies
has traditionally relied on the expertise of medical professionals, who perform 
visual assessments. However, due to the limited number of such experts available,
there is an urgent need to develop a pre-diagnostic tool capable of early detection
of abnormal neurological behaviors.
The primary objective of our research project is to create a computational tool that
leverages unsupervised learning methods to detect abnormal motor behaviors in newborns.
In our previous work, we employed pose estimation models to extract numerical skeletons
from 2D videos captured using conventional cameras.
Our current work involves utilizing attention mechanisms for forecasting multivariate 
time series data. We aim to train a model on adult motricities before fine-tuning it 
to predict normal newborn motor behaviors and, in doing so, highlight deviations that
may indicate abnormal neurological conditions.
