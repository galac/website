Title: Self-Stabilization and Byzantine Tolerance for Maximal Independent Set
date: 2021-10-22 14:00
slug: 2021-10-22-team-seminar
Authors: Jonas Sénizergues
lang:en
institution: LISN, Galac
tags: Team seminar, graphs
location: LRI, 445

summary: 
We analyze the impact of transient and Byzantine faults on the construction of a maximal independent set in a general network. We adapt the self-stabilizing algorithm presented by Turau for computing such a vertex set. Our algorithm is self-stabilizing, and also works under the more difficult context of arbitrary Byzantine faults. Byzantine nodes can prevent nodes close to them from taking part in the independent set for an arbitrarily long time. We give boundaries to their impact using a variation on the notion of containment radius. As far as we know, we present the first algorithm tolerating both transient and Byzantine faults under the fair distributed daemon. We prove that this algorithm converges in O(n) rounds with high probability.
