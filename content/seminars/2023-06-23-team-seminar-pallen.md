Title: Games on Tilings
date: 2023-06-23 14:00
slug: 2023-06-23-team-seminar
Authors: Rémi Pallen
lang:en
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: Given a finite set A of colors and a finite set of target patterns F,
 to know if one can tile the infinite grid avoiding patterns in F is the domino
 problem. This problem can be seen as a one-player game, where the goal for the
 player is to tile the grid. In this talk, we consider a two-player version of
 this game, where each player chooses in turn a color for a cell; the maker A
 wants to create a target pattern and the breaker B wants to avoid them. <br>
 In this internship, I am studying which player has a winning strategy, depending
 on colors, target patterns and the turn order.