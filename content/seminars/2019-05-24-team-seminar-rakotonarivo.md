Title: Sur le nombre des (d,k)-polytopes
date: 2019-05-24 14:30 
slug: 2019-05-24-Team-seminar 
Authors: Rado Rakotonarivo
lang:fr
institution: LIPN, University of Paris 13
tags: Team seminar, combinatorics 
location: LRI, 455

**Résumé :** 

Un polytope est l'enveloppe convexe d'un ensemble fini de points dans un espace euclidien. On dénotera par (d,k)-polytope un polytope entier de dimension d de R^d et contenu dans l'hypercube [0,k]^d. Ce sont des objets faciles à décrire mais dont la complexité est accrue dès que la dimension augmente. Relativement peu de choses sont connues sur ces objets et notamment leur nombre qui est exponentiel en d. Dans cet exposé, il sera proposé deux algorithmes qui comptent les (d,k)-polytopes sans pour autant les énumérer mais dont la particularité est de ne pas calculer d'enveloppe convexe. Un premier algorithme pour le cas où k = 1 et un deuxième pour k > 1. Il sera également présenté de nouveaux résultats obtenus avec les implémentations des dits algorithmes.
