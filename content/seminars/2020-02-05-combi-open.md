Title: Séminaire ouvert
date: 2020-02-05 10:30
slug: 2020-02-05-combi-seminaire
Authors: Toute l'équipe
lang:fr
institution: LIX et GALAC
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Lors d'un séminaire ouvert, le thème n'est pas décidé à l'avance. Tous les membres du séminaires sont invités à participer et peuvent proposer le jour même des interventions plus ou moins longues, des démos ou des questions ouvertes au reste de l'équipe.


