Title: Vingt mille lieues sous les mots
date: 2022-04-22 14:00
slug: 2022-04-22-team-seminar
Authors: Pierre Béaur
lang:fr
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: Dans cette présentation, nous découvrirons une partie de l'écosystème de 
l'océan de la combinatoire de mots, dont l'exploration a commencé depuis 
plus d'un siècle. À la surface vivent d'abord les mots périodiques, aux 
propriétés très régulières et simples : ils nous serviront de point 
d'ancrage. Nous plongerons ensuite dans des eaux plus complexes pour 
observer les mots substitutifs, qui peuvent avoir des comportements plus 
exotiques : apériodicité, récurrence linéaire, complexité quadratique... 
Enfin, nous descendrons à des niveaux plus profonds, où la lumière 
n'a pas encore atteint tous les recoins, pour étudier les 
représentations S-adiques. Je parlerai des monstres qu'elles peuvent 
générer, des problèmes ouverts autour des représentations S-adiques, 
ainsi que de certains premiers résultats et outils pour mieux les 
appréhender.
<br>
[Slides utilisées lors de la présentation](/documents/SeminaireGalacBeaur.pdf)
