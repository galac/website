Title: Flips sur les triangulations de la sphère : une borne inférieure pour le temps de mélange
date: 2017-05-24 11:00
slug: 2017-05-24-combi-seminaire
Authors: Thomas Budzinski
lang:fr
authorurl: https://www.eleves.ens.fr/home/budzinsk/
institution: ENS & Paris-Sud 
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

 Une des manières les plus naturelles de simuler une triangulation uniforme de la sphère à n faces est d'utiliser une méthode de Monte-Carlo : on démarre avec une triangulation quelconque puis, de manière répétée, on choisit une arête uniformément et on la "flippe", i.e. on l'efface et on la remplace par l'autre diagonale du quadrilatère qui se forme. On montrera que le temps de mélange de la chaîne de Markov obtenue est au moins en $n^{5/4}$.

