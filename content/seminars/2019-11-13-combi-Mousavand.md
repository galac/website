Title: Brauer-Thrall Conjectures, Old and New!
date: 2019-11-13 10:30
slug: 2019-11-13-combi-seminaire
Authors: Kaveh Mousavand
lang:en
institution: 
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

$\tau$-tilting theory is an elegant-- but technical-- subject in representation theory of associative algebras, with motivations from cluster algebras. It was introduced by Adachi-Iyama-Reiten, in 2014. However, thanks to the recent result of Demonet-Iyama-Jasso, one can fully phrase the concept of $\tau$-tilting finiteness in terms of linear algebra. I adopt this elementary approach to share some new results on $\tau$-tilting finiteness of several families of algebras with rich combinatorics.

I begin with a review of two fundamental conjectures by Brauer and Thrall. After some historical remarks on those, I present a gentle introduction to $\tau$-tilting finiteness, which allows me to state a conjecture of similar nature that relies on my recent work on $\tau$-tilting theory. I will share my main strategy, as well as my new results on some cases.
*I will not assume any prior knowledge of representation theory of algebras!*



