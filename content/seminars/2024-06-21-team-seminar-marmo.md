Title: Travo: a classroom open source Python toolkit
date: 2024-06-21 14:00
slug: 2024-06-21-team-seminar
Authors: Chiara Marmo
lang:en
institution: LISN
tags: Team seminar, networks
location: LRI, 445

summary: Teaching computer science or computational courses inevitably implies collaboration on code.
 Software forges can provide helpful infrastructure to support and improve collaboration in teaching
 workflows. This talk will present Travo, an open source Python toolkit turning your favorite forge
 into a flexible management solution for computer assignments. Travo is a collaboration between 
 "Université Paris Saclay" and "Université du Québec à Montréal". It is used in production in a dozen
 large and smaller classes there.<br>
This talk will describe the motivations underneath the development of Travo and its technical stack. 
The summary of the developments implemented during the last months will be presented, which is leading 
to the release of the first stable version of the software.  
