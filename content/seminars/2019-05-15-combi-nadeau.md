Title: Classe de cohomologie de la variété de Peterson
date: 2019-05-15 11:00
slug: 2019-05-15-combi-poincare-seminaire
Authors: Philippe Nadeau
lang:fr
institution: ICJ, Université Lyon 1
tags: Combi seminar, combinatorics
location: Salle Henri Poincaré du LIX

La variété de Peterson est une sous-variété importante de la variété de drapeaux complets, et possède en tant que telle une classe de cohomologie que l'on peut alors développer dans la base des classes de Schubert. Les coefficients sont des entiers négatifs car ils représentent certains nombres d'intersection, mais les déterminer explicitement est délicat avec des méthodes géométriques.
Notre but dans ce travail est de donner une interprétation combinatoire pour ces coefficients. Dans cet exposé qui se veut accessible à tous, nous utilisons une approche algébrique et combinatoire pour donner des réponses partielles à ce problème et formuler plusieurs conjectures.  Polynômes de Schubert et mots réduits de permutations jouent des rôles clés.
Travail en commun avec Vasu Tewari (UPenn).
