Title: Manytamaris: on descriptions of the Tamari lattice
date: 2024-10-11 10:00
slug: 2024-10-11-team-seminar
Authors: Hoan La
lang:fr
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: Manytamaris is a website about the Tamari lattice. The interest in this particular lattice
 stems from its many properties, besides being a lattice, and its numerous appearances in different
 areas of mathematics. As such, the goal for Manytamaris is to be a survey on the descriptions of
 the Tamari lattice with many contributors. Furthermore, since Manytamaris is a website, it can be
 seen as a constantly updated and accessible survey.<br>
Currently, the wiki shows how the Tamari lattice can be defined on Catalan objects; most notably the
 original definition by Huang and Tamari in 1972. To show that it is indeed the same lattice defined 
 over these objects, the wiki presents some bijections and poset isomorphisms.<br>
 I will present what is currently on the wiki and what is planned for the future.
