Title:  Une généralisation de la correspondance RSK via des représentations de carquois (de type A)
date: 2023-09-18 11:00
slug: 2023-09-18-combi-seminar
Authors: Benjamin Dequene 
lang:fr
institution: UQAM
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

La correpondance de Robinson-Schensted-Knuth est une bijection partant des matrices d'entiers naturels vers les paires de tableaux de Young semi-standards. Une version généralisée donne une bijection entre des remplissages d'un tableau d'une certaine forme, et les partitions planes renversés de la même forme. 

D'un point de vue "représentation de carquois", la correspondance RSK donne une bijection entre deux invariants particuliers d'un module X (dans une certaine catégorie). Les entrées d'un remplissage arbitraire correspondent aux multiplicités des facteurs indécomposables de X, tandis que les entrées de la partition plane renversée enregistrent la donnée générique de Jordan de X, un invariant introduit par Alexander Garver, Rebecca Patrias et Hugh Thomas.

Mon exposé a pour but de présenter une version un peu plus général de cette correspondance, en y incluant une interaction avec un choix arbitraire d'une orientation d'un carquois de type A, correspondant à un choix d'un élément de Coxeter dans Sn. Pour cet exposé, aucune grande connaissance de la theorie des représentations de carquois ne sera nécessaire.

C'est une partie de mon travail de thèse, encadré par Hugh Thomas.

