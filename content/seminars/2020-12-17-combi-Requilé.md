Title: Du modèle d'Ising sur les cartes à l'énumération de certaines classes de graphes planaires 
date: 2020-12-17 14:00
slug: 2020-12-17-combi-seminaire
Authors: Clément Requilé
lang:en
institution: 
tags: Combi seminar, combinatorics
location: https://bbb.lri.fr/b/jer-k22-eqk

https://bbb.lri.fr/playback/presentation/2.0/playback.html?meetingId=39bbfe5742a821b16c1d6669a8ef00da2d072c73-1608209247910

Un graphe est étiqueté quand son ensemble de sommets est
{1,...,n}, et planaire s'il admet un plongement sur la sphère. Une
carte (planaire) est définie comme un plongement particulier. Dans ce
contexte, le modèle d'Ising représente l'ensemble des 2-coloriages des
sommets d'une carte. Ces coloriages ne sont pas nécessairement propres
dans le sens où on autorise les arêtes monochromes. En termes
combinatoires, on s'intéresse à la fonction génératrice d'Ising,
associée à une famille de cartes, qui compte toutes les cartes
2-coloriées de la famille avec un poids u^m, où m est le nombre
d'arêtes monochromes. Bernardi et Bousquet-Mélou ont (entre autres)
prouvé en 2011 que la fonction génératrice d'Ising est algébrique
pour la famille des cartes et pour celle des triangulations.

Dans cet exposé, on va discuter comment on peut appliquer ce
résultat à l'énumération de certaines classes de graphes planaires.
En premier lieu les graphes planaires bipartis étiquetés. Puis si le
temps le permet, les graphes planaires cubiques étiquetés enrichis
d'un couplage parfait. Ce dernier point s'avère instrumental pour
dériver l'espérance du nombre de couplages parfaits dans un graphe
planaire cubique aléatoire. Ce travail est en collaboration avec Marc
Noy et Juanjo Rué. 

