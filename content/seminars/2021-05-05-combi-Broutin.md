Title: L’arbre brownien parabolique
date: 2021-05-05 10:30
slug: 2021-05-05-combi-seminaire
Authors: Nicolas Broutin
lang:en
institution: Sorbonne Universite
tags: Combi seminar, combinatorics
location: https://bbb.lri.fr/b/jer-k22-eqk

Je présenterai une construction explicite d'un arbre réel aléatoire à partir d'un mouvement brownien avec drift parabolique. L'objet obtenu est intimement lié au graphes aléatoires et au coalescent multiplicatif, et est distribué comme la limite d'échelle de l'arbre couvrant minimal du graphe complet. On y voit aussi clairement les connections avec le coalescent additif. C'est un travail en collaboration avec Jean-François Marckert. 

