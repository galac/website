Title: Permutahedral matchings, zonotopal tilings, and d-partitions
date: 2019-11-20 10:30
slug: 2019-11-20-combi-seminaire
Authors: Cesar Ceballos
lang:en
institution: 
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

In this talk I will present higher dimensional generalizations of the following three concepts:

* (a) perfect matchings of a hexagonal tiling,
* (b) rhombus tilings of a hexagon, and
* (c) plane partitions.

I will show that these generalizations are equivalent under certain specific bijections. The generalizations of (b) and (c) have been already studied in the literature but, as far as I know, the generalization of (a) appears to be new. 
This is work in progress based on discussions with Tomack Gilmore and Vivien Ripoll.



