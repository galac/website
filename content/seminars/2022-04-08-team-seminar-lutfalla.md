Title: Strong local rules without labels for Penrose rhombus tilings
date: 2022-04-08 14:00
slug: 2022-04-08-team-seminar
Authors: Victor Lutfalla
lang:fr
institution: Aix-Marseille Université
tags: Team seminar, combinatorics
location: LRI, 445

summary: The Penrose rhombus tilings are a subshift of tilings of the
    planefirst defined by R. Penrose.
    This subshift is crucial in the study of geometrical tilings
    because, though it was originally defined with 2 rhombus tiles with
    cuts-and-notches or arrows on the edges, it is an aperiodic subshift
    with local 10-fold rotational symmetry and it also is a substitution
    subshift and a cut-and-project subshift.
    We prove that the Penrose rhombus tilings can be defined by local
    rules without labels (the arrows or cuts-and-notches on the edges
    of  the tiles). We present such a set of local rules.
    Note that the fact that there exists a set of local rules without
    labels is known, but we were unable to find a clear reference or an
    actually correct set of local rules.
<br>
[Slides utilisées lors de la présentation](../documents/Lutfalla.pdf)