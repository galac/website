Title: Bijections for tree-decorated maps and applications to random maps
date: 2019-03-13 14:00
slug: 2019-03-13-combi-poincare-seminaire
Authors: Luis Fredes
lang:fr
institution: LaBRI, Bordeaux
tags: Combi seminar, combinatorics
location: Salle Henri Poincaré du LIX

We introduce a new family of maps, namely tree-decorated maps where the tree is not necessarily spanning. To study this class of maps, we define a bijection which allows us to deduce combinatorial results, recovering as a corollary some results about spanning-tree decorated maps, and to understand local limits. Finally, we discuss the possible metric scaling limit of this map: the shocked map. We give certain properties and give the conjectured continuum construction. This is a work in progress with Avelio Sepúlveda.
