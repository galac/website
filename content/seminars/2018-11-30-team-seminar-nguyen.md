Title: Some recent results on the integer linear programming formulation for the Max-Cut problem
date: 2018-11-30 14:30 
slug: 2018-11-30-Team-seminar 
Authors: Hung Nguyen 
lang:fr
institution: GALAC, LRI
tags: Team seminar, graphs 
location: LRI, 455

**Summary:**  Given an undirected graph G=(V,E) where the edges are weighted by an arbitrary cost vector c, a cut S in G associated with a node subset S is the edge subset of E which contains all the edges having exactly one end-node in S.
The Maximum Cut problem which is to find a  maximum total weight cut in G is one of the fundamental problems in combinatorial optimization.
The problem is NP-hard and has applications in many areas such as statistical physics, VLSI circuit design, scheduling, \ldots
Exact solutions for Max-Cut are usually based on mathematical formulations such as SDP for dense graphs and integer linear programming for sparse graphs.
The latter has  non-compact formulations using cycle inequalities in the original space and compact (i.e., polynomial size) extended formulations using O(n^3) triangle inequalities defined on the complete graph of n vertices. 
In this talk, we show that one can simply reduce the number of triangle inequalities to O(nm).
We also present several extensions of the result for special sparse graphs and for graph partitioning problem.
