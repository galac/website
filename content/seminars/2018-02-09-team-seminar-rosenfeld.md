Title: Binary pattern of length greater than 14 are abelian-2-avoidable
date: 2018-02-09 14:30 
slug: 2018-02-09-Team-seminar 
Authors: Matthieu Rosenfeld
lang:fr
institution: GALAC, LRI
tags: Team seminar, combinatorics 
location: LRI, 445


**Summary:** Two words u and v are abelian equivalent if they are permutation of each other ("aabc" and "baca" are abelian equivalent). Let w be a word and P= P1...Pn (where the Pi are the letters of P) a pattern (a word over another alphabet), we say that w contains an abelian occurrence of P if there exist non-empty words w1,...,wn such that w1...wn is a factor of w and for all i and j, Pi =Pj implies that wi and wj are abelian equivalent. A pattern is avoidable if there is an infinite word that does not contain it.
In 1992 Keränen answered a question from Erdös and showed that there is an infinite word over 4 letters that avoids the pattern AA in the abelian sens. More recently it was showed that every binary pattern of length greater than 118 is avoided in the abelian sens by a binary word. Here we improve the 118 to 14 with a computer assisted proof. 
