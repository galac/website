Title: Polytopes of independent sets of relations and their 1-skeleta
date: 2018-04-11 11:00
slug: 2018-04-11-combi-seminaire
Authors: Nantel Bergeron
lang:en
authorurl: http://www.math.yorku.ca/bergeron/
institution: York University
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

With Farid Aliniaeifard, Carolina Benedetti,  Nantel Bergeron,  Shu Xiao Li and   Franco Saliola.

We characterize the edges of two classes of $0/1$-polytopes whose
vertices encode the ``independent sets'' of a relation on a finite set.
The first class includes poset chain polytopes, the vertex packing polytopes
from graph theory, some instances of matroid independence polytopes, as
well as newly-defined polytopes whose vertices correspond to noncrossing set partitions.
In analogy with matroid basis polytopes, the second class is obtained by
considering the independent sets of maximal cardinality.
