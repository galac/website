Title: Asymptotic distribution of parameters in random maps
date: 2019-02-06 11:00
slug: 2019-02-06-combi-seminaire
Authors: Sergey Dovgal
lang:en
institution: LIPN Univ. Paris 13
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

In this joint work with Olivier Bodini, Julien Courtiel, and
Hsien-Kuei Hwang, we consider random rooted maps without regard to their 
genus. We address the problem of limiting distributions for six 
different parameters:
- vertices
- leaves
- loops
- root edges
- root isthmic constructions
- root vertex degree
Each parameter has a different limiting distribution, varying from 
(discrete) geometric and Poisson to (continuous) Beta, normal, uniform, 
and an unusual bounded distribution characterised by its moments.

