Title:  Polytopes : théorèmes importants et généralisations conjecturales sur des espaces tropicaux
date: 2023-11-27 11:00
slug: 2023-11-27-combi-seminar
Authors: Matthieu Piquerez
lang:fr
institution: Univ. de Nantes
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Certains théorèmes importants concernant les polytopes, par exemple le
g-théorème et le problème de Minkowski, se trouvent à l'interaction de
domaines des mathématiques dont on ne soupçonnerait pas la diversité au
premier abord. Divers résultats plus ou moins récents en géométrie
algébrique, en géométrie tropicale ou en théorie de Hodge combinatoire
laissent penser qu'on pourrait interpréter et améliorer ces résultats en
généralisant la notion de polytopes et les théorèmes sus-cités à des
espaces polyédraux dits tropicaux.

Dans cet exposé, je vais rappeler ces jolies théorèmes, tenter de donner
un aperçu informel des divers interactions et de leurs importances, et
discuter des idées, questions et difficultés rencontrées concernant les
généralisations attendues.
