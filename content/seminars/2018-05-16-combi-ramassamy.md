Title: Extensions d'ordres cycliques partiels, boustrophédons et polytopes
date: 2018-05-16 11:00
slug: 2018-05-16-combi-seminaire
Authors: Sanjay Ramassamy
lang:fr
authorurl: http://www.normalesup.org/~ramassamy/index.html.fr
institution: UMPA, ENS Lyon
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX


Tandis que l'énumeration des extensions linéaires des ensembles
partiellement ordonnés a fait l'objet de nombreux travaux, son analogue
cyclique (énumération des extensions à des ordres cycliques totaux d'un
ordre cyclique partiel donné) a été fort peu étudié. Dans cet exposé,
j'introduirai certaines classes d'ordres cycliques partiels pour lesquels
ce problème d'énumération est tractable. Certains cas font appel à une
version multidimensionnelle de la construction du boustrophédon, un
triangle de nombres utilisé pour calculer les nombres zigzags d'Euler.
J'expliquerai aussi le lien entre ces questions d'énumération d'extensions
d'ordres cycliques partiels et le calcul du volume de certains polytopes.

Il s'agit d'un travail en partie en commun avec Arvind Ayyer (Indian
Institute of Science) et Matthieu Josuat-Vergès (LIGM / CNRS)
