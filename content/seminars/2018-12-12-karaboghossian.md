Title: Invariant polynomial et théorème d’inversion sur le monoïde de Hopf des hypergraphes
date: 2018-12-12 11:00
slug: 2018-12-12-combi-seminaire
Authors: Théo Karaboghossian
lang:fr
institution: Labri, Université de Bordeaux
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

La notion de monoïde de Hopf a été introduite par Aguiar et Mahajan et formalise de façon algébrique les notions de fusion et séparation d’objet combinatoires (concaténation de mots, restriction de graphes etc). Aguiar et Ardila ont montré que ce formalisme donne un cadre idéal pour définir des invariants polynomiaux. Dans cet exposé je présente la notion de monoïde de Hopf et m’appuie sur les travaux d’Aguiar et Ardila pour définir un nouvel invariant polynomial d’hypergraphe. Je montre ensuite que cet invariant est sujet à un théorème d’inversion similaire à celui de Stanley sur le polynôme chromatique d’un graphe. Finalement je montre que l’on retrouve des résultats similaires déjà connus en tant que conséquences de ce théorème.

