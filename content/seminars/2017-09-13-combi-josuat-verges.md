Title: Ordre de Belinschi et Nica sur les partitions non croisees
date: 2017-09-13 11:00
slug: 2017-08-13-combi-seminaire
Authors: Matthieu Josuat-Vergès
lang:fr
authorurl: http://igm.univ-mlv.fr/~josuatv/
institution: IGM
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Travail en collaboration avec  Philippe Biane.

In the context of noncommutative probabilty theories, Belinschi and Nica introduced an order on noncrossing partitions, which is stronger than the usual refinement order. Our goal is to investigate its generalization to finite Coxeter groups. The motivation is that a generating function of its intervals is related with the Möbius function of the usual order. Via Chapoton's ex-conjectures F=H and H=M, it connects this enumeration of intervals with that of faces in the cluster complex and antichains in the root poset.

