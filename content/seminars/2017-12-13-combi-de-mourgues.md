Title: Une approche combinatoire pour les dynamiques de type Rauzy
date: 2017-12-13 11:00
slug: 2017-12-13-combi-seminaire
Authors: Quentin de Mourgues
lang:en
authorurl: 
institution: LIPN, Université Paris 13
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Les dynamiques de type Rauzy sont des actions de groupes (ou de monoide) sur une collections d’objets combinatoires. L’exemple le plus connu (la dynamique de Rauzy) concerne une action sur les permutations, associée aux transformations d’échanges d’intervalles (IET) pour l'application de Poincaré sur les surfaces de translations orientables. Les classes d’équivalences sur les objets induites par l’action de groupe sont reliées aux composantes connexes de l’espace de module des differentielles abéliennes avec un ensemble de singularités donné, et ont été classifiées par Kontsevich et Zorich, et par Boissy, en utilisant des éléments de théorie de géométrie algébrique, de topologie, de systèmes dynamiques et de combinatoires.

Dans ce séminaire je présenterai le contexte géométrique et dynamique de la dynamique de Rauzy puis je définirai de facon générale les dynamiques de type Rauzy et j'introduirai une méthode de preuve (la méthode d’étiquetage) pour caractériser les classes d'équivalence de ces dynamiques. 
Pour finir j'illusterai quelques étapes de la méthode d'étiquetage sur une dynamique d'involution dont les classes d'équivalence sont en bijection avec les graphes eulériens.

