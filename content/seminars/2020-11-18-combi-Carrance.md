Title: Énumération de cartes bicolorées au bord exotique
date: 2020-11-18 10:30
slug: 2020-11-18-combi-seminaire
Authors: Ariane Carrance
lang:en
institution: Université Paris-Saclay
tags: Combi seminar, combinatorics
location: https://bbb.lri.fr/b/jer-amh-gpd

Les cartes bicolorées peuvent être vues comme un cas particulier du modèle d'Ising. Ainsi, dans le cas d'une condition au bord monochromatique par exemple, très habituelle pour le modèle d'Ising, leur énumération est bien connue. Cependant, elles apparaissent aussi dans certains modèles de cartes aléatoires, avec des conditions au bord plus exotiques, telles qu'un bord dit alternant. Je présenterai dans cet exposé un travail en cours en collaboration avec Jérémie Bouttier, où nous utilisons des outils de combinatoire analytique pour énumérer les cartes bicolorées à bord alternant, en nous concentrant particulièrement sur les m-angulations et les m-constellations.


