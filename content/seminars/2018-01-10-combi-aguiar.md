Title: Topics in hyperplane arrangements
date: 2018-01-10 11:00
slug: 2018-01-10-combi-seminaire
Authors: Marcelo Aguiar
lang:en
authorurl: http://www.math.cornell.edu/~maguiar/
institution: Cornell University
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

We will discuss a number of geometric and algebraic
constructions associated to real hyperplane arrangements, focusing on
the monoid of faces and the category of lunes of the arrangement. We
will then discuss the beginnings of a theory of noncommutative Mobius
functions and its connections to the structure of the algebra of
faces.

If time allows, we will also discuss an extension of a theorem of
Joyal, Klyachko and Stanley relating the homology of the partition
lattice to free Lie algebras.

These topics are from joint work with Swapneel Mahajan published last
month in the Mathematical Surveys and Monographs of the AMS. No
previous knowledge of hyperplane arrangements will be assumed.
