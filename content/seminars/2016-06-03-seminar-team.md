Title: Classer des discussions en ligne : la structure d'arbre est-elle suffisante ?
date: 2016-06-03 14:30 
slug: 2016-06-03-seminar 
Authors:  Matias Mano
lang:en 
institution: LRI 
tags: Team seminar 
location: LRI, 435, salle des theses

Abstract : We are interested in open online discussion forums. A discussion could be seen as a tree (defined by graph theory) with an "Original Poster" (being the root of the tree) and members of the forum who discuss on the first post (being the branch). We are looking for specific features of trees. Literature provides us with results on inherent tree factors (width, depth, degree distribution). Firstly, we verify these results on our database. Later, we will introduce external variables, such as user experience (adding a value on vertices) to produce a more expressive model, taking into account all dimensions of the data."
