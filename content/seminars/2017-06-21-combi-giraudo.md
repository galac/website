Title: Algèbres de Hopf combinatoires des pros
date: 2017-07-05 11:00
slug: 2017-07-05-combi-seminaire
Authors: Samuele Giraudo
lang:fr
authorurl: http://igm.univ-mlv.fr/~giraudo/
institution: Paris-Est Marne-la-Vallée
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Un pro est une structure algébrique dont les objets sont des opérations à plusieurs entrées et plusieurs sorties. Ils généralisent en un certain sens les opérades dans lesquelles les opérations n'ont qu'une seule sortie. Si bon nombre de liens entre la théorie des opérades et la combinatoire ont été développés ces derniers temps, ceux entre les pros et la combinatoire ne demandent qu'à être tissés. Nous présentons ici une construction associant à chaque pro libre une algèbre de Hopf combinatoire. Cette construction s'étend à des pros non nécessairement libres, appelés pros rigides. En plus de fournir de nouvelles algèbres de Hopf sur différents objets combinatoires (diverses forêts, forêts d'arbres/anti-arbres, empilements), elle offre une construction alternative des déformations à un paramètre de Foissy de l'algèbre de Faà di Bruno.

