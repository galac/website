Title: On the intervals of framing lattices
date: 2024-12-12 14:00
slug: 2024-12-12-team-seminar
Authors: Loïc Le-Mogne
lang:fr
institution: LaBRI
tags: Team seminar, combinatorics
location: LRI, 445

summary: A flow graph $G$ is an acyclic oriented graph with $V(G) = [n]$, $E(G)$ a
multi-set of edges where each edge $(i,j)$ satisfies $i<j$, and such that $G$
has a unique source $s=1$ and sink $t=n$. On such a graph, a route is
simply a path from $s$ to $t$. We can then define a relation of
compatibility between two routes (we will in fact define multiple
compatibilities on a fixed graph; each compatibility will be called a
framing). A framing lattice is a poset (and more precisely, as its
name implies, a lattice) defined on the maximal cliques of coherent
routes on a flow graph.<br>
This work first aimed to prove a conjecture from Clément Chenevière,
stating that the number of linear intervals (intervals with only
comparable elements) does not change depending on the framing chosen
on a flow graph. While this result was proven during this project, we
are trying to study how, on a fixed flow graph, 2D-faces of the
lattices change when we change the framing.<br>
This is an on-going project with Clément Chenevière, Sergio A.
Fernández de soto, Félix Gélinas, and Germain Poullot.
