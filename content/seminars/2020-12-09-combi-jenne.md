Title: Combinatorics of the dP3 Quiver
date: 2020-12-09 10:30
slug: 2020-12-09-combi-seminaire
Authors: Helen Jenne
lang:en
institution: Univ. Tours
tags: Combi seminar, combinatorics
location: https://bbb.lri.fr/b/jer-amh-gpd

https://bbb.lri.fr/playback/presentation/2.0/playback.html?meetingId=39bbfe5742a821b16c1d6669a8ef00da2d072c73-1607504566916

For the past several years, Tri Lai, Gregg Musiker, and others have studied the quiver associated to the del Pezzo 3 surface and its associated cluster algebra, with the goal of providing combinatorial interpretations for toric cluster variables. They proved that in most cases the Laurent expansions of toric cluster variables agree with generating functions for dimer configurations of certain subgraphs of the dP3 lattice. Their proof uses the fact that the generating function for dimer configurations of a planar bipartite graph satisfies a recurrence called Kuo condensation, named for its resemblance to Dodgson condensation.

However, in some cases, the question of how to combinatorially interpret the toric cluster variables remained unanswered. In this talk, I discuss ongoing joint work with Lai and Musiker which shows that in these cases, the Laurent expansions agree with generating functions for tripartite double-dimer configurations. 

