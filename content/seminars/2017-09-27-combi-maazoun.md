Title: Limite d'échelle des permutations séparables : description et universalité du permuton séparable Brownien
date: 2017-09-27 11:00
slug: 2017-09-27-combi-seminaire
Authors: Mickaël Maazoun
lang:fr
authorurl: http://perso.ens-lyon.fr/mickael.maazoun/
institution: UMPA, ENS Lyon
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Travaux en collaboration avec F. Bassino, M. Bouvel, V. Feray, L. Gerin et A. Pierrot.
Le permuton séparable Brownien a été introduit par Bassino et. al. (2016) comme la limite d'échelle des permutations séparables. C'est une mesure aléatoire sur le carré unité,  construite à partir d'une excursion Brownienne décorée de signes indépendants et uniformes en chaque minimum local. Nous montrons une version plus directe de cette construction, qui permet d'identifier les propriétés géométriques (dimension de Hausdorff, autosimilarité) du support, et met au jour un lien avec un réordonnancement aléatoire de l'arbre continu Brownien.
Dans un second temps, nous parlerons des limites possibles pour les classes de permutations fermées par substitution, généralisations naturelles des permutations séparables.