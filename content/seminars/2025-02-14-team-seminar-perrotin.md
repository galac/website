Title: Alternating and nondeterministic plane-walking automata
date: 2025-02-14 14:00
slug: 2025-02-14-team-seminar
Authors: Pacôme Perrotin
lang:fr
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: Plane-walking automata were introduced by Salo & Törma to recognise languages
 of two-dimensional infinite words (subshifts), the counterpart of 4-way finite automata
 for two-dimensional finite words. We extend the model to allow for nondeterminism and 
 alternation of quantifiers. We prove that the recognised subshifts form a strict subclass
 of sofic subshifts, and that the classes corresponding to existential and universal 
 nondeterminism are incomparable and both larger than the deterministic class. We define
 a hierarchy of subshifts recognised by plane-walking automata with alternating quantifiers,
 which we conjecture to be strict.