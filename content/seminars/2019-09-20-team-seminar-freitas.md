Title: Scalable Load Balancing - Distributed Algorithms and the Packing Model
date: 2019-09-20 14:30
slug: 2019-09-20-team-seminar
Authors: Vinicius Freitas
lang:en
tags: Team seminar, graphs
location: LRI, 445

summary: Load imbalance is a recurring problem in High Performance Computing (HPC), which leads to suboptimal performance via the under-use of available resources. 
As computing systems grow larger, resource management and load balancing become a costly process, especially for dynamic applications that demand periodical workload balance.
With this in mind, we believe that future generation load balancing algorithms should look towards scaling along computing systems.
In order to express solutions to the aforementioned issues, we propose a distributed scheduling model based on large-scale parallel machines and HPC systems.
Additionally, we present a task packing model to minimize the decision costs of distributed algorithms, and present two scheduling strategies implemented in the Charm++ runtime system that use said model.
