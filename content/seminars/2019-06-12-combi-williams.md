Title: Independence Posets
date: 2019-06-12 11:00
slug: 2019-06-12-combi-seminaire
Authors: Nathan Williams
lang:en
institution: UT Dallas
tags: Combi seminar, combinatorics
location: Salle Henri Poincaré du LIX

Let G be an acylic directed graph. For each vertex of G, we define an involution on the independent sets of G.  We call these involutions flips, and use them to define a new partial order on independent sets of G.

Trim lattices generalize distributive lattices by removing the graded hypothesis: a graded trim lattice is a distributive lattice, and every distributive lattice is trim. Our independence posets are a further generalization of distributive lattices, eliminating also the lattice requirement: an independence poset that is a lattice is always a trim lattice, and every trim lattice is the independence poset for a unique (up to isomorphism) acyclic directed graph G.  This is joint work with Hugh Thomas.
