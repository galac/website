Title:  Expérimentations sur les séries génératrices des groupes des tresses
date: 2021-01-14 14:00
slug: 2021-01-14-combi-seminaire
Authors: Jean Fromentin
lang:fr
institution: Université du Littoral
tags: Combi seminar, combinatorics
location: https://bbb.lri.fr/b/jer-k22-eqk

https://bbb.lri.fr/playback/presentation/2.0/playback.html?meetingId=02b8d542f7dedeb822e4e461ae6aab733cec4e88-1610628981813

Dans cet exposé, je présenterai de nouveaux outils algorithmiques pour
étudier les séries génératrices
sphériques et géodésiques des groupes des tresses relativement aux
générateurs d'Artin ou de Birman-Ko-Lee.
Je finirai par une présentation des résultats obtenus et par la
formulation de conjectures pour les groupes
des tresses à 3 et 4 brins relativement aux générateurs de
Birman-Ko-Lee.

