Title: A round trip from crystal bases to integer partitions
date: 2020-03-04 10:30
slug: 2020-03-04-combi-seminaire
Authors: Isaac Konan
lang:fr
institution: IRIF, Université Paris Diderot
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

The representation theory of Lie algebras occurs as a rich source of partition identities. This started with Lepowsky and Wilson's proof of Rogers-Ramanujan identities via the representation of level 3 standard module for the affine type $A_1^(1)$. A good example of an identity generator is the $(KMN)^2$ crystal base character formula. Starting from this result, Primc gave two identities on the character of the standard module L(\Lambda_0) for the affine types $A_1^(1)$ and $A_2^(1)$.
In recent works with Dousse, our starting points are the identities given by Primc.
We first built a family of colored partitions with $n^2$ colors, whose cases $n=2$ and $n=3$ respectively give the Primc partitions for affine $A_1^(1)$ and $A_2^(1)$. We then related those Primc generalized partitions to another family of combinatorial objects, the generalized Frobenius partitions, and were able to give nice explicit formulas for their generating functions. Then, we moved back to crystal base theory by proving that our Primc generalized partitions were the corresponding objects for the affine $A_{n-1}^(1)$. This allowed us to give an explicit non-dilated formula of the characters, not only for  L(\Lambda_0) but for all the level 1 standard modules L(\Lambda_l) with $l\in\{0,\cdots,n-1\}$,  as a sum of $(n-1)!$ series with obvious positive coefficients.
