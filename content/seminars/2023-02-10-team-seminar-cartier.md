Title: Lattice properties of acyclic pipe dreams
date: 2023-02-10 14:00
slug: 2023-02-10-team-seminar
Authors: Noémie Cartier
lang:en
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: Pipe dreams were introduced by Bergeron and Billey in order to study Schubert polynomials
and encode the algebraic structure of the symmetric group. In particular, with well-chosen parameters,
they have the combinatorial structure of the Tamari lattice, a well-known quotient of the weak order
on permutations. In this presentation we will give some more general conditions resulting in pipe dreams
sets isomorphic to quotients of weak order intervals in the symmetric group.
