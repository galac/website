Title: Enumération des cartes planaires à trois bords par découpage en tranches
date: 2021-11-15 15:00
slug: 2021-11-15-combi-seminaire
Authors: Emmanuel Guitter
lang:fr
institution: Institut de Physique Théorique IPhT
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet, LIX, École Polytechnique

Parmi toutes les techniques d’énumération des cartes planaires (graphes plongés sur la sphère à deux dimensions), une des approches conceptuellement les plus simples et directes consiste à découper la carte en tranches: si on dispose d’une règle canonique de découpage et que les tranches ainsi obtenues sont faciles à énumérer, le tour est joué. Je montrerai comment cette méthode, longtemps limitée aux cartes à deux bords (c-à-d deux faces ou sommets marqués) au plus, peut-être étendue au cas des cartes planaires à trois bords. Cela requiert l’introduction de nouveaux types de tranches, a priori plus délicates à énumérer, mais dont le comptage effectif n’est en fait pas nécessaire et peut être contourné par une petite astuce… Ceci est un travail en commun avec Jérémie Bouttier et Grégory Miermont.   
