Title: Three interacting families of Fuss-Catalan posets
date: 2020-01-22 10:30
slug: 2019-12-11-combi-seminaire
Authors: Camille Combe
lang:en
institution: IRMA, Strasbourg
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

We will introduce three families of posets depending on a nonnegative integer parameter $m$, 
having underlying sets enumerated by the $m$-Fuss Catalan numbers. 
Among these, one is a generalization of Stanley lattices and another one is a generalization of Tamari lattices. We will see how these three families of posets are related in several ways. Indeed, they fit into a chain for the order extension relation and they share some properties. Moreover, we will notice some isomorphisms of posets between specific elements of these posets. Then, we will introduce two associative
algebras built as quotients of generalizations of the Malvenuto-Reutenauer algebra. Their products describe intervals of our analogues of Stanley lattices and Tamari lattices. In particular, we will see that one is a generalization of the Loday-Ronco algebra. 

This is a joint work with S. Giraudo.
