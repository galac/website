Title: A new determinant for the Q-enumeration of alternating sign matrices 
date: 2018-10-31 11:00
slug: 2018-10-31-combi-seminaire
Authors: Florian Aigner
lang:en
institution: University of Vienna
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

We prove a determinantal formula for the $Q$-enumeration of
alternating sign matrices (ASMs),
i.e. a weighted enumeration where each ASM is weighted by $Q$ to the
power of the number of its $-1$'s.
Evaluating this determinant leads to closed product formulas and new
proofs of the $1$-,$2$- and $3$-enumeration of ASMs.
Finally we relate our results to the determinant evaluations of Ciucu,
Eisenkölbl, Krattenthaler and Zare (2001),
which count weighted cyclically symmetric lozenge tilings of a hexagon
with a triangular hole.
