Title: Distributions explicites associées au mouvement brownien indexé par l'arbre brownien
date: 2020-03-11 10:30
slug: 2019-03-11-combi-seminaire
Authors: Armand Riera
lang:en
institution: LMO, Orsay
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Le mouvement brownien indexé par l'arbre brownien est l'analogue continue
des marches aléatoires indexées par des arbres de Galton-Watson critiques
(de variances finies). Il est notamment relié au Super-mouvement brownien
ainsi qu'aux limites d'échelles de cartes aléatoires (de petites faces).
Le but de l'exposé est de présenter certaines distributions explicites de
fonctionnelles de cet objet. En particulier, on donnera une preuve directe
dans le continue d'un résultat de Bousquet-Mélou et Janson donnant la loi
de la densité en 0 du ISE.

La méthode pour obtenir ces résultats mélange des outils de théorie de
processus ainsi que de combinatoire. Cet exposé est issu d'un travail en
collaboration avec Jean-François Le Gall.
Aucun prérequis ne sera nécessaire.

