Title: Étude énumérative des intervalles dans les treillis de type Tamari
date: 2024-01-12 14:00
slug: 2024-01-12-team-seminar
Authors: Clément Chenevière
lang:en
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: Le treillis de Tamari est un ordre partiel sur les objets Catalan. De nombreuses descriptions
 de ce treillis donnent lieu à de nombreuses familles de généralisations, notamment les treillis m-Tamari,
 nu-Tamari et m-Cambriens. Après une "visite guidée" dans ce zoo des généralisations du treillis de Tamari,
 je présenterai mon travail de thèse.<br>
Je parlerai en particulier d'une nouvelle famille d'ordres partiels appelés alt nu-Tamari, généralisant le
 treillis de Tamari, et de l'étude de leurs intervalles linéaires, ainsi que d'une conjecture de Stump-Thomas-Williams
 stipulant que le nombre d'intervalles dans le treillis m-Cambrien en type A linéaire est le même que dans
 le treillis m-Tamari, et de mes avancées sur ce sujet.
