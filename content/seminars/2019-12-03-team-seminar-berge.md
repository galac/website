Title: PHD defense: A guide book for the traveller on graphs full of blockages
date: 2019-12-03 14:00
slug: 2019-12-03-team-seminar
Authors: Pierre Bergé
lang: en
institution: LRI, University of Paris Saclay
tags: Team seminar, graphs
location: CentraleSupelec, amphi IV

summary: 
We study NP-hard problems dealing with graphs containing blockages.

We analyze cut problems via the parameterized complexity framework. The
size p of the cut is the parameter. Given a set of sources {s_1,...,s_k}
and a target t, we propose an algorithm deciding whether a cut of size
at most p separates at least r sources from t. This problem is called
Partial One-Target Cut. Our algorithm is FPT. We also prove that the
vertex version of the problem, where the cut contains vertices instead
of edges, is W[1]-hard. Our second contribution is an algorithm counting
minimum (S,T)-cuts in time 2^{O(p \log p)}n^{O(1)}.

Then, we present numerous results on the competitive ratio of
deterministic and randomized strategies for the Canadian Traveller
Problem. We show that randomized strategies which do not use memory
cannot improve the ratio 2k+1. We also provide theorems on the
competitive ratio of randomized strategies in general. We study the
distance competitive ratio of a group of travellers with and without
communication. Eventually, we treat the competitiveness of deterministic
strategies dedicated to certain families of graphs. Two strategies with
a ratio below 2k+1 are proposed: one for equal-weight chordal graphs and
another one for graphs such that the largest minimal (s,t)-cut is at
most k.
