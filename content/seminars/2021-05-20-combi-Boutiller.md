Title: Modèles de dimères sur graphes minimaux : au-delà du cas elliptique
date: 2021-05-20 14:00
slug: 2021-05-20-combi-seminaire
Authors: Cédric Boutillier
lang:en
institution: Sorbonne université
tags: Combi seminar, combinatorics
location: https://bbb.lri.fr/b/jer-k22-eqk

Les modèles de dimères sur les graphes planaires ont fait leur début en
tant qu'objet d'étude mathématique avec les travaux de Kasteleyn dans
les années 1960. Au début des années 2000, d'importants résultats
théoriques sont démontrés, dont deux résultats dans des directions
différentes :
- la construction du diagramme de phases pour les modèles de dimères
  sur graphes planaires bipartis bipériodiques, et les propriétés
  universelles de ces phases (Kenyon, Okounkov, Sheffield)
- la "localité" de l'"inverse" de la matrice de Kasteleyn qui sert à
  calculer les corrélations entre dimères pour une famille de graphes
  planaires particuliers : les graphes "isoradiaux" critiques (Kenyon).

Dans une série de travaux en collaboration avec David Cimasoni (Genève)
et Béatrice de Tilière (Dauphine), nous généralisons ces deux types de
résultats dans un cadre unifié. En travaillant sur une famille de
graphes un peu plus générale que les graphes isoradiaux, sans hypothèse
de périodicité, et étant donné une surface de Riemann compact, nous
construisons des familles de poids pour le modèle de dimères sur ces
graphes et décrivons le diagramme de phase. Toutes les mesures obtenues
ont la propriété de localité observées par Kenyon dans le cas critique.

