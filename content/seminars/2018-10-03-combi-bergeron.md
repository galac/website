Title:Comprendre la combinatoire des fonctions “parking" rectangulaire via les espaces de polynômes harmoniques diagonaux (exposé 1)
date: 2018-10-03 11:00
slug: 2018-10-03-combi-seminaire
Authors: François Bergeron
lang:fr
institution: UQAM
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Dans cette suite d’exposés, j’ai l’intention d’expliquer comment de nouveaux espaces de polynômes multivariés permettent: de rendre compte de la riche combinatoire des généralisations rectangulaires des fonctions parking, des chemins de (m,n)-Catalan, et des treillis de (m,n)-Tamari; de comprendre les familles de fonctions symétriques issues de l’étude des opérateurs de l’algèbre de Hall elliptique, et de mettre en évidence leurs symétries; d’unifier les divers énoncés des variantes de la conjecture Delta; de comprendre les symétries des super-polynômes des (m,n)-noeuds/entrelacs sur le tore; etc. Tout cela ouvre la porte à de nombreux nouveaux problèmes allant de la combinatoire énumérative, à la théorie de la représentation, en passant par la théorie des fonctions symétriques et la physique théorique (pour n’en citer que deux). Plusieurs nouvelles conjectures seront discutées en cours de route. Au besoin (selon les souhaits par l’auditoire) toutes les notions préliminaires nécessaires pourront être discutées. Pour la majorité des notions abordées, des outils de calcul formel sont disponibles ou en cours d'élaboration. 
