Title: Modular Automata Networks
date: 2023-06-02 14:00
slug: 2023-06-02-team-seminar
Authors: Pacôme Perrotin
lang:en
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: Automata networks are finite dynamical systems used to model gene regulatory networks.
 In this talk we talk about the difficult task of characterizing their limit behavior, and explore
 the formalism of modules which aims to simplify some of the technicalities related to those tasks.
