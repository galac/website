Title: Cardinal d'un ensemble de coupure minimal en percolation de premier passage
date: 2019-11-27 10:30
slug: 2019-11-27-combi-seminaire
Authors: Marie Théret
lang:en
institution: Université Paris Nanterre
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

On considère le modèle de percolation de premier passage sur $\mathbb{Z}^d$ en dimension $d\geq 2$ : on associe aux arêtes du graphe une famille de variables i.i.d. positives ou nulles. On interprète la variable aléatoire associée à une arête comme étant sa capacité, i.e., la quantité maximale d'eau ou d'information qui peut la traverser par seconde. Il en découle une définition naturelle de flux maximal à travers une région bornée du graphe entre un ensemble d'émetteurs et un ensemble de récepteurs. Ce flux maximal est égale à la capacité minimale d'un ensemble de coupure, c'est-à-dire un ensemble d'arêtes tel que si on le retire du graphe on disconnecte totalement les émetteurs des récepteurs. Dans cet exposé, on s'intéressera à une des caractéristiques de cet ensemble de coupure minimal : son cardinal. Il s'agit d'un travail en collaboration avec Barbara Dembin (LPSM, Université de Paris)



