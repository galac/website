Title: The coloring problem in clique-hypergraphs of graphs
date: 2016-05-04 10:30
slug: 2016-05-04-10-30-seminar
Authors: Pr. Liying Kang
lang:en
institution: Department of Mathematics, Shanghai University, China
tags: Plateau seminar
location: LRI



ABSTRACT:  A maximal clique of a graph is a clique not properly contained in
any other clique. A $k$-clique coloring of a graph is an assignment of a $k$
colors to the vertices of $G$ such that no maximal clique with at least two
vertices is monochromatic. The clique-chromatic number of $G$ is the smallest 
color number k such that G exists a $k$-clique-coloring. In this talk 
we give a survey on clique-coloring of graphs.
