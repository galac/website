Title: Éléments minimaux des régions de Shi
date: 2022-10-14 14:00
slug: 2022-10-14-team-seminar
Authors: Balthazar Charles
lang:fr
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: L'arrangement de Shi, introduit en 1987 par J.-Y. Shi, est un arrangement d'hyperplans liés aux groupes de
réflexions cristallographiques possédant de nombreuses propriétés intéressantes. En particulier, les régions définies
par les hyperplans liés à un groupe W correspondent aux états d'un automate introduit par B. Brink et R. B. Howlett
capable de reconnaître le "langage des éléments de W" (plus exactement de leur mots réduits). <br>
Motivés par la recherche d'automates minimaux (C. Hohlweg, P. Nadeau et N. Williams 2016), puis par des questions similaires 
dans les groupes de tresses (M. Dyer, C. Hohlweg 2016), une poussée a été faite ces dernières années par Chapelier-Laget, Dyer, Fishel, Hohlweg...
pour comprendre les éléments minimaux des régions de Shi. <br>
Le but de cet exposé est de présenter une description de ces éléments minimaux, ainsi qu'une réponse (positive) à la conjecture 2 de
(M. Dyer, C. Hohlweg, 2016) dans le cas des groupes de Weyl affines. Les notions nécessaires de groupes de réflexions, système de
racines, groupes cristallographiques, arrangement de Shi, seront présentées au cours de l'exposé. Les résultats exposés seront 
accompagnés de leur interprétation dans le cadre du groupe symétrique.
