Title: Des tresses aux amas via la dualité de Koszul
date: 2019-02-27 11:00
slug: 2019-02-27-combi-seminaire
Authors: Matthieu Josuat-Vergès
lang:fr
institution: UPEM
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Il est bien connu que le groupe de tresses admet une présentations avec des générateurs qui satisfont les relations de tresses.  On peut voir ces générateurs comme échangeant deux brins voisins du point de vue géométrique.  Une autre présentation, due à Birman, Ko, Lee, consiste à regarder un ensemble plus gros de générateurs (ils correspondent alors à toutes les transpositions et pas seulement aux transpositions simples).  Un problème d'énumération des tresses selon leur longueur (nombre de facteurs dans une expression en termes de produits de générateurs) fait intervenir les partitions non-croisées et leur fonction de Möbius, d'après des résultats de Albenque et Nadeau.  Après avoir introduit ces résultats, nous expliquerons pourquoi ceci mène à considérer la notion de dual de Koszul, et pourquoi cette dualité fait un lien avec le complexe d'amas.  Nous éviterons les détails techniques, en particulier on ne considèrera que le cas du groupe symétrique bien qu'en général le cadre est celui des groupes finis de réflexions.  Il s'agit d'un travail en commun avec Philippe Nadeau.
