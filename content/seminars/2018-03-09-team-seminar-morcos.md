Title: A Two-level Auction for Resource Allocation in Multi-tenant C-RAN
date: 2018-03-09 14:30 
slug: 2018-03-09-Team-seminar 
Authors: Mira Morcos
lang:fr
institution: GALAC, LRI
tags: Team seminar, networks
location: LRI, 445


**Summary:** Next generation (5G) mobile networks are targeting twenty five-fold data rates provided by the current generation of mobile networks, with higher efficiency, enhanced mobility support and seamless management of connected devices. In order to provide such features, at reduced Capital Expenditure (CAPEX) and Operational Expenditure (OPEX), the Cloud- RAN paradigm has been recently proposed.

The C-RAN architecture is based on two key features: (1) Centralization, wherein
computational resources of base stations, namely Base Band Units (BBUs), are pooled
together in a central Cloud, and (2) Virtualization, with the possibility that several Mobile
Virtual Network Operators (MVNOs) share the radio resources and the BBUs in order to
reduce physical resources costs and maintenance. This evolution poses however multiple
challenges, especially in terms of dynamic resource allocation between the users as well as
between the MVNOs.

We propose in this presentation a dynamic resource allocation scheme between several
Mobile Virtual Network Operators (MVNOs), sharing common radio resources at a Cloud-
based Radio Access Network (C-RAN) run by a central operator. We specifically propose a
two-level coupled auction so as to enhance resource utilization and maximize the revenues
both for the central operator and the MVNOs: at the lower level, end users belonging to a
given MVNO bid for resources and, at the higher-level, MVNOs compete for resources at
the central operator based on the output of the lower-level auction. We show fundamental
economic properties of our proposal: truthfulness and individual rationality, and propose a
greedy algorithm to enhance its computational efficiency. We prove the existence of Nash
equilibrium for the global auction and its uniqueness in a typical duopoly scenario.
