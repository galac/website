Title: Intervalles linéaires dans les treillis de Dyck, Tamari et alt-Tamari.
date: 2022-10-17 11:00
slug: 2022-10-17-combi-seminaire
Authors: Clément Cheneviere
lang:fr
institution: Unistra
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Les treillis de Dyck et de Tamari sont des ordre partiels classiques sur
les objets Catalan, et on peut notamment les définir sur les chemins de
Dyck. Ces deux posets ne possèdent pas le même nombre d'intervalles,
mais de façon surprenante, lorsque l'on ne regarde que leurs intervalles
linéaires (c'est-à-dire totalement ordonnés) il s'avère qu'ils
apparaissent en même nombre, et ce, même en les distinguant selon leur
hauteur (c'est-à-dire leur nombre d'éléments).
Dans cet exposé, on présentera les deux treillis et comment compter
leurs intervalles linéaires, puis on introduira une toute nouvelle
famille de posets, appelés treillis alt-Tamari, qui contient les
treillis de Dyck et de Tamari. Tous les posets de la famille possèdent
eux aussi le même nombre d'intervalles linéaires et c'est ce qui
explique cette coïncidence étonnante.
Si le temps le permet, on évoquera une généralisation de ce résultat au
cas nu-Tamari.

