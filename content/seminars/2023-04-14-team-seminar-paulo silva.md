Title: Auction mechanisms for allocation and pricing in Edge Computing
date: 2023-04-14 14:00
slug: 2023-04-14-team-seminar
Authors: João Paulo Silva
lang:en
institution: LISN, Galac
tags: Team seminar, networks
location: LRI, 445

summary: Soon, billions of devices will be connected to the Internet, and most of them
 will need external resources to store and process their data. Edge Computing can benefit
 these devices by making these resources available at the network's edges to be closer to 
 the user. However, these offered resources must be managed to be allocated efficiently.
 Auction mechanisms from Algorithmic Game Theory allow users to be modeled as buyers of Edge
 Computing resources being auctioned. To follow this approach, it is necessary to guarantee
 certain properties of an auction mechanism that are difficult to treat from a computational
 point of view. It is necessary to seek approximate allocation alternatives so that the auction
 is efficient while guaranteeing the maximization of resource usage. Therefore, the purpose
 is to develop approximate allocation mechanisms that are computationally efficient and have
 guarantees of maximizing the use of resources being offered in Edge Computing.


