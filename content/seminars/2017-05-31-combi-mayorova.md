Title: Un nouveau lien entre l’algèbre de descente du groupe hyperoctaèdral, les tableaux de dominos et les fonctions quasisymétriques de Chow
date: 2017-05-31 11:00
slug: 2017-05-31-combi-seminaire
Authors: Alina Mayorova
lang:fr
authorurl: http://www.lix.polytechnique.fr/Labo/alina.mayorova/
institution: LIX & Univ. Moscou
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Introduite par Solomon dans son article de 1976, l’algèbre de descente d’un groupe de Coxeter fini a reçu une attention significative au cours des années passées. Gessel a montré dans le cas du groupe symétrique que ses constantes de structure donnent la table de comultiplication de la base fondamentale des fonctions quasisymétriques. Nous montrons que cette propriété implique en réalité plusieurs relations connues liées à la correspondence Robinson-Schensted-Knuth et certaines de ses généralisations. Ceci donne un nouveau lien entre ces résultats et la théorie des fonctions quasisymétriques et permet d’obtenir de nouvelles formules plus avancées faisant intervenir les coefficients de Kronecker. Par ailleurs en utilisant la théorie des fonctions quasisymétriques de type B introduite par Chow nous pouvons étendre notre méthode au cas du groupe hyperoctaèdral et démontrer de nouvelles formules.


