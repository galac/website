Title: Block gluing in Hom shifts and path reconfiguration in graphs
date: 2022-06-10 10:30
slug: 2022-06-10-team-seminar
Authors: Benjamin Hellouin de Menibus
lang:en
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: 
We study some tilings spaces that are defined from graph homomorphisms, called Hom shifts.
Compared to general tiling spaces, they look the same in every direction (invariance by rotation and symmetry)
and many undecidable problems or questions in tiling spaces seem to become easier for these objects, using graph-theoretical methods.
<br>
This talk is about gluing distance: given two n x n admissible patterns (=partial homomorphisms),
how far do they need to be so that we can glue them in an admissible larger pattern?
The problem can be restated in terms of distance between paths in graphs under some specific reconfiguration step.
The proofs use various graph-theoretical tools, including some notions of path deformation (homotopy) and algebraic constructions.
<br>
For most simple Hom shifts, this distance is constant or linear;
however, we produce a more elaborate example with logarithmic gluing distance. 
We conjecture (are in the process of proving) that constant, linear, and logarithmic distances are the only three possible behaviours,
and that this makes an argument for these behaviours to be "combinatorially natural" for general tiling spaces.
<br>
Ongoing work with Silvère Gangloff and Piotr Opocha.