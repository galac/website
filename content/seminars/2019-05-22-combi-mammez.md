Title: Mots tassés stricts croissants, fonctions quasi-symétriques et fonctions symétriques non commutatives; éléments primitifs
date: 2019-05-22 11:00
slug: 2019-05-22-combi-seminaire
Authors: Cécile Mammez
lang:fr
institution: Laboratoire J.A. Dieudonné, Univ. de Nice Sophia-Antipolis
tags: Combi seminar, combinatorics
location: Salle Henri Poincaré du LIX

Une algèbre de Hopf est un espace vectoriel muni d’une structure de bigèbre
(ie d’une structure d’algèbre et de cogèbre avec une relation de compatibilité) et d’un
antimorphisme d’algèbres particulier appelé antipode.
L’objectif de cet exposé est d’expliquer les connections entre les mots tassés stricts
croissants d’une part et les algèbres de Hopf des fonctions quasi-symétriques QSym et
des fonctions symétriques non commutatives NSym d’autre part ainsi que d’en déduire
des éléments primitifs de NSym. Pour cela, nous commencerons par rappeler la notion
d’algèbre de Hopf ainsi que les définitions de QSym et NSym.

Nous expliquerons ensuite la construction des mots tassés stricts croissants et expliciterons sa structure d’algèbre de Hopf. Nous décrirons une familles d’éléments primitifs. Nous montrerons que son dual gradué est isomorphe à l’algèbre de Hopf des fonctions quasi-symétriques. Ainsi, l’algèbre de Hopf des mots tassés stricts croissants est

isomorphe à celle des fonctions symétriques non commutatives.
La dernière partie de l’exposé sera consacrée à l’explicitation d’un isomorphisme
explicite entre les mots tassés stricts croissants et NSym pour en déduire des éléments
primitifs de cette dernière.
