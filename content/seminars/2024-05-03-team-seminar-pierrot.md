Title: Ranking aggregation: graph-based methods and use in bioinformatics
date: 2024-05-03 14:00
slug: 2024-05-03-team-seminar
Authors: Adeline Pierrot
lang:en
institution: LISN, Galac
tags: Team seminar, combinatorics
location: LRI, 445

summary: The problem of ranking aggregation is the following: we have a set of
elements and a set of rankings of these elements as input, and we want a
single ranking as output, which best reflects the set of rankings taken
as input. The applications are manifold, especially in bioinformatics,
and the resolution techniques are numerous, each with their own
advantages and disadvantages. In this talk, we will present some of the
graph-based resolution methods developed in recent works in the
bioinformatics team of the LISN.