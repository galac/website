Title: Une série pour la constante connective du réseau carré 
date: 2018-11-21 11:00
slug: 2018-11-21-combi-seminaire
Authors: Pierre-Louis Giscard
lang:fr
institution: LMPA, Université du littoral Côte d'Opale
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Nous montrerons comment, à l’aide d’un crible de la théorie des nombres, il est possible d’obtenir une série convergeant vers la constante connective μ dictant la croissance asymptotique du nombre de polygones auto-évitants d'un réseau régulier. Nous détaillerons la mise en oeuvre théorique et pratique du crible, notamment la nécessité de le faire fonctionner sur la densité des empilements de cycles d’un graphe plutôt que leur nombre. Ces considérations nous conduirons à mieux comprendre le terme dominant et les termes d’erreurs du crible, nous permettant finalement de calculer les 4 et 5 premiers ordres de la série donnant μ à 0.2% puis 0.06% près sur le réseau carré. Enfin, nous verrons ce qu’il reste à faire pour trouver la correction polynomiale sur le nombre de polygônes auto-évitants, qui nous échappe encore. 
