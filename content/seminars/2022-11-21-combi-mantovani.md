Title: A realization of poset associahedra as sections of graph associahedra
date: 2022-11-21 11:00
slug: 2022-11-21-combi-seminar
Authors: Chiara Mantovani
lang:fr
institution: LIX
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Poset associahedra are a family of convex polytopes introduced by Pavel Galashin in 2021, each one associated to a partially ordered set, that generalize the classical associahedron. Galashin describes the combinatorial structure of poset associahedra, and he realizes them as convex polytopes. However, his construction is not completely satisfactory. In particular, it does not provide explicit integer coordinates.
Another class of well known generalizations of the associahedron is graph associahedra, each one associated to a graph. Graph associahedra were introduced by Carr and Devadoss and have several geometric realizations.
In this talk I will describe the combinatorial structure of both graph associahedra and poset associahedra. I will then present Postnikov’s realization of graph associahedra as generalized permutahedra, and I will use it to provide a new realization of the poset associahedron of a poset P, obtained as a section of the graph associahedron of the line graph of P.
