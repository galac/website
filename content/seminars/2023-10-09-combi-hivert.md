Title:  Réalisation de l'algèbre d'Okada et correspondance de Robinson-Schensted-Fomin du treillis de Young-Fibonacci
date: 2023-10-09 11:00
slug: 2023-10-09-combi-seminar
Authors: Florent Hivert
lang:fr
institution: LISN
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Il est bien connu que le treillis de Young peut s'interpréter comme le
diagramme de Bratelli des groupes symétriques, décrivant, par exemple, comment
les représentations irréductibles se restreignent de Sn à S_n-1. En 1975,
Stanley a découvert un treillis similaire appelée treillis de Young-Fibonacci
qui a été interprété comme le diagramme de Bratelli d'une famille d'algèbres
par Okada en 1994.

Dans cet exposé, nous réalisons l'algèbre d'Okada et le monoïde associé grâce
à une version étiquetée des diagrammes d'arcs du monoïde de Jones et de
l'algèbre de Tempeley-Lieb. Ceux-ci nous permettent de prouver en toute
généralité que l'algèbre d'Okada est de dimension n! — la preuve d'Okada
n'étant valide que dans le cas semi-simple. En particulier, nous interprétons
la bijection naturelle entre les permutations et les diagrammes d'arcs comme
une instance de la correspondance de Robinson-Schensted-Fomin associée au
treillis de Young-Fibonacci. Disposant maintenant d'un moyen de calculer dans
les monoïdes et algèbres d'Okada, nous pouvons étudier en détail leurs
structures et par exemple prouver que le monoïde est apériodique et décrire
ses relations de Green. En relevant, ces dernières à l'algèbre nous en
construisant une base cellulaire.

