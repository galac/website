Title: Théorème de rigidité et fonctions de Parking 
date: 2018-03-21 11:00
slug: 2018-03-21-combi-seminaire
Authors: Bérénice Delcroix-Oger
lang:fr
authorurl: https://www.irif.fr/~bdelcroix/
institution: IRIF, Paris 7 Diderot
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Une question classique, mais difficile, de combinatoire algébrique est de savoir si une algèbre d'un type donné est libre sur l'ensemble de ses générateurs. Après avoir introduit tous les prérequis, j'expliquerai en quoi les théorèmes de rigidité pour les opérades, introduits en 2008 par Loday et récemment étendus, permettent d'apporter une réponse à ce genre de problème, notamment en ce qui concerne les algèbres des fonctions de Parking et des surjections. Ce travail a été fait en collaboration avec Emily Burgunder.
