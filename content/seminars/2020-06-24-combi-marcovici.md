Title: Corrélations discrètes d’ordre 2 de certaines suites automatiques
date: 2020-06-24 10:30
slug: 2020-06-24-combi-seminaire
Authors: Irène Marcovici
lang:fr
institution: Université de Lorraine
tags: Combi seminar, combinatorics
location: à distance


Résumé : Une suite k-automatique est une suite qui peut être calculée par un automate fini de la manière suivante : le n-ième terme de la suite est fonction de l'état atteint par l’automate après lecture de la représentation de l'entier n en base k. Ces suites peuvent également être obtenues à partir du point fixe d'une substitution de longueur k.
Je montrerai qu'il existe des familles de suites automatiques qui, malgré leur description très simple, ont les mêmes corrélations d'ordre 2 qu'une suite i.i.d. de symboles choisis uniformément au hasard. Plus précisément, pour tout entier r>0, et pour tout couple (i,j) de symboles, la proportion asymptotique d'entiers n pour lesquels (u_n,u_{n+r})=(i,j) est égale à 1/L^2, où L est le nombre de symboles. La preuve repose sur des ingrédients simples et se généralise à des suites multi-dimensionnelles.
