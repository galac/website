Title: Excluding a rectangular grid
date: 2024-11-14 14:00
slug: 2024-11-14-team-seminar
Authors: Clément Rambaud
lang:fr
institution: Université Côte d'Azur
tags: Team seminar, graphs
location: LRI, 445

summary: For every positive integer k, we define the k-treedepth as the largest graph parameter td_k satisfying (i) td_k(&varnothing;)=0; (ii) td_k(G) <= 1+ td_k(G-u) for every graph G and every vertex u; and (iii) if G is a (<k)-clique-sum of G_1 and G_2, then td_k(G) ≤ max{td_k(G_1), td_k(G_2)}, for all graphs G_1, G_2. This parameter coincides with treedepth if k=1, and with treewidth plus one if k ≥ |V(G)|. We prove that for every positive integer k, a class of graphs C has bounded k-treedepth if and only if there is a positive integer l such that for every tree T on k vertices, no graph in C contains T □ P_l as a minor.
<br>
This implies for k=1 that a minor-closed class of graphs has bounded treedepth if and only if it excludes a path, for k=2 that a minor-closed class of graphs has bounded 2-treedepth if and only if it excludes a ladder (Huynh, Joret, Micek, Seweryn, and Wollan; Combinatorica, 2021), and for large values of k that a minor-closed class of graphs has bounded treewidth if and only if it excludes a grid (Grid-Minor Theorem, Robertson and Seymour; JCTB, 1986).
<br> 
As a corollary, we obtain the following qualitative strengthening of the Grid-Minor Theorem in the case of bounded-height grids. For all positive integers k, l, every graph that does not contain the k × l grid as a minor has (2k-1)-treedepth at most a function of (k, l).