Title: L-orientations of graphs
date: 2019-12-06 14:30
slug: 2019-12-06-team-seminar
Authors: Kenta Ozeki
lang:en
institution: Yokohama National University
tags: Team seminar, graphs
location: LRI, 475

summary: An orientation of an (undirected) graph G is an assignment of directions to each edge of G.
In this talk, we consider an orientation such that the out-degree of each vertex is contained in a given list.
We introduce several relations to graph theory topics and pose our main conjecture, together with some partial solutions.
This is joint work with S. Akbari, M. Dalirrooyfard, K.Ehsani, and R. Sherkati.

