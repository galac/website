Title: Un groupe diédral agissant sur les mots de Dyck de même longueur
date: 2017-12-06 11:00
slug: 2017-12-06-combi-seminaire
Authors: Robert Cori
lang:en
authorurl: http://www.labri.fr/perso/cori/
institution: Labri, Université de Bordeaux
tags: Combi seminar, combinatorics
location: Salle Philippe Flajolet du LIX

Dans les années 70 on disait que pour comprendre les propriétés d’une
structure mathématique il était bon d’étudier un groupe de transformations agissant dessus. Il est donc étonnant de ne trouver aucune approche de ce type pour les mots (ou chemins) de Dyck.
Dans un travail récent avec M. Barnabei, F. Bonetti et N. Castronuovo nous avons très partiellement comblé ce manque en introduisant deux involutions sur ces mots. Je me propose de vous raconter les résultats obtenus (lors de cette collaboration) pour le groupe qu’elles engendrent. Je tenterai ensuite de justifier mon intérêt pour ces involutions en faisant référence à un travail en commun avec Y. Le Borgne sur le rang des configurations du jeu du « chip firing » (ou du tas de sable) sur le graphe complet.

