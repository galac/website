#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'default'
SITENAME = u'GALaC'
SITEURL = ''
SITESUBTITLE = u'Graphs, Algorithmic and Combinatorics'
SITESEO = u'GALaC LRI Paris-Sud'

PATH = 'content'
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = u'en'

THEME = 'theme'

PLUGIN_PATHS = ['plugins/']
PLUGINS = [
    'i18n_subsites',
    'pelican-page-order',
    'edit-page-link',
    'attach-members-page',
    'drop-down-menu',
    'articles-on-page',
    'render_math',
    'summary'
]

INDEX_SAVE_AS = 'blog_index.html'

STATIC_PATHS = ["images","documents",'js','php']

I18N_SUBSITES = {
    'fr': {
        'SITENAME': u'GALaC',
        'SITESEO': u'GALaC LRI Paris-Sud',
        'SITESUBTITLE' : u'Graphes, Algorithmique et Combinatoire'
        }
    }

I18N_UNTRANSLATED_ARTICLES = 'keep'
I18N_UNTRANSLATED_PAGES = 'keep'

DISPLAY_CATEGORIES_ON_MENU = False

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = None

# Social widget
SOCIAL = None

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# loading members info
import json

# gitlri Edit page info

EDITLINKBASE = "https://gitlri.lri.fr/galac/website/blob/master/"

