# Site web de l'équipe GALaC

Ce projet contient les fichiers sources du site web de l'équipe GALaC. Le site 
utilise le *générateur de site statique* [Pelican](http://blog.getpelican.com/).
C'est-à-dire que les fichiers mis en ligne sont en simple *HTML* créés à partir 
des fichiers sources présents ici.

## Principe général, première contribution

Tous les fichiers sources du site sont accessibles sur le serveur *gitlri.lri.fr*
à travers un répertoire partagé sous *git*. 

**Je ne me suis jamais connecté à gitlri, comment contribuer au site ?** 

* Première étape : cliquez sur le lien "Sign in" en haut à droite de l'écran et 
connectez-vous au site avec vos identifiants du LRI.

* Deuxième étape : envoyez un email à viviane.pons@lri.fr pour qu'elle vous ajoute 
à l'équipe GALaC et vous donne les droits de modifications sur le siteweb.

**J'ai suivi ces étapes, maintenant comment éditer une pages ?**

* A partir d'une page du site cliquez sur le lien *Edit this page* en bas à droite

* Vous arrivez sur le fichier contenu correspondant à la page sur gitlri. Cliquez 
  sur le bouton "Edit" qui se trouve au dessus du contenu à droite (Pas de bouton edit ? 
  Connectez vous grâce au lien "Sign in" en haut à droite de la page)

* Effectuez la modification, pour plus de détails, cf section [Contenu du site](#contenu-du-site).

* Entrez un message résumant la modification dans le champs "Commit message" 
  (ce message sera public et accessible dans l'historique de la page sur gitlri)

* Cliquez sur le bouton vert "Commit changes", vous devez voir apparaître le message 
  "Your message has been sucessfully commited".

* Le changement sera visible sur le site dans les 15 minutes suivant la modification.

Remarque : le serveur gitlri conserve l'historique du site au fur et à mesure des 
modifications. Si vous avez supprimé des informations par erreur, il sera toujours 
possible de les retrouver. N'hésitez pas à demander de l'aide : viviane.pons@lri.fr.

## Contenu du site

**Pour accéder aux fichiers source, cliquez dans le menu de gauche sur *files*.**

L'ensemble des fichiers relatifs au contenu de base du site sont stockés dans le
répertoire *content/*. Il existe deux sortes de contenu, les [Pages](#pages) dans le 
dossier *content/pages/* et les [Articles](#articles-séminaires-et-news) (Séminaires et news) dans les
dossiers *content/seminars/* et *content/news/*. Les dossiers *content/images* et 
*content/documents* contiennent les fichiers statiques (images et documents) qui
peuvent être utilisés dans les pages ou les articles.

### Pages

Les pages sont stockées dans *content/pages*. Pour chaque page du site, on trouve 
un fichier *nom-de-la-page.md* écrit en format *Markdown* qui sera utilisé pour 
engendrer la page html correspondante.

#### L'entête du fichier, les informations relatives à la page

La première partie du fichier contient les informations relatives à la page, sous 
la forme

```
attribut1: valeur
attribut2: valeur
```

Voici une liste des attributs courants utilisés sur le site. Ceux marqué d'une * sont obligatoires.

| Attribut	| Signification		| Exemple					 |
| --- | --- | --- |
| Title*	| Titre de la page 	| SageMath, an open-source mathematical software |
| Authors       | les auteurs de la page | Sylvie Delaët |
| menutitle	| Titre utilisé dans le menu (si différent de Title) | Sage          |
| slug*         | identifiant de la page (sans espace ni majuscle) | sage |
| lang*		| langue de la page (cf [Traductions](#traductions))	| en |
| img 		| Une image associée à la page (cf [Images](#images-documents-média)) | combinatorics.png |
| thumb_img     | La vignette associée à la page lorsque cette dernière est utilisée comme sous-page (facultatif) | combinatorics-thumb.png |
| description   | Un texte court de description | Sage is an open source mathematical software to which the GALaC team contributes a lot. |
| page_order	| l'ordre dans le menu | 1|
| parentpage	| le `slug` de la page parente si on est sur une sous-page (cf [Menu](#menu-sous-menu)) | softwares |
| members       | Une liste de membres GALaC associés à la page (cf [Membres associsés à une page](#membres-associés-à-une-page)  | Sylvie Delaët, Viviane Pons | 
| include_catgeory | afficher sur la page les article d'une certaine catégorie (cf [Articles associés à une page](#articles-associés-à-une-page) | seminars |


#### Le contenu du fichier

Vient ensuite le contenu du fichier à proprement parler. Il est écrit en texte simple
utilisant le format **Markdown** et offre des possibilités de mise en page basique :
paragraphes, gras, italique, titres, etc. 

La [page suivante](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) 
résume bien les possibilités de la syntaxe Markdown.

#### Images, documents, média

Il est possible d'insérer des images ou des liens vers des documents. Pour cela,
il faut d'abord uploader votre image / document sur le site.

* Cliquez sur "Files" dans le menu de gauche sur gitlri.
* Choisissez le répertoire "content" puis "images" ou "documents"
* Cliquez sur le bouton "+" au dessus de la liste des fichiers, puis sur "Upload file"
* Choisissez l'image à uploader sur votre machine, complétez le "Commit message" 
  puis cliquez sur "Upload file".

Une fois que votre image est uploadée, vous pouvez la choisir comme image de présentation
de la page grâce à l'attribut "img". Ou alors, vous pouvez l'insérer ou vous voulez dans la page avec

```
![Image Title](/images/image_name.jpg)
```

Pour insérer un document, c'est

```
[Nom du document](/documents/nom_document.pdf)
```

(Remarquez le point d'exclamation avant les crochets pour l'image qui les différencient
d'un simple lien). 

Les images possèdent quelques options de mises en page que voici.

Image centrée

```
![Image Title](/images/image_name.jpg){.center}
```

Image alignée à gauche (le texte se mettra à droite) 

```
![Image Title](/images/image_name.jpg){.left}
```

Image alignée à droite (le texte se mettra à gauche) 

```
![Image Title](/images/image_name.jpg){.right}
```

#### Latex

Il est possible d'insérer de simples formules mathématiques à l'aide de latex à 
l'intérieur du texte Markdown. 

```
Voici une formule inline $\frac{1+\sqrt{5}}{2}$. Et sur sa propre ligne :

$$$\frac{1+\sqrt{5}}{2}$$$
```


#### Traductions

Le site de GALaC propose pour l'instant deux langues : anglais et français. Les pages
en anglais sont dans le dossier *content/pages/* et les pages en français dans *content/pages/fr/*.

Supposons qu'une page existe en anglais mais pas en français, alors sa version *non traduite*
apparaîtra sur le sous-site en français et vice-versa. 

**Comment rajouter une traduction manquante ?**

Attention ! Si vous éditez la page non traduite, vous êtes en train d'éditer la page 
originale et non pas de rajouter une traduction. Pour chaque traduction, il faut créer
une **nouvelle page**.

Voici la procédure pour ajouter la traduction en français d'une page en anglais

* Cliquez sur le lien "Edit this page" de la page en anglais sur le site de GALaC.
* Cliquez sur le bouton "Edit" sur gitlri puis *sélectionnez* l'ensemble du contenu
et le copier (bouton droit copier ou Ctr+V)
* Cliquez sur le lien "Files" dans le menu de gauche de gitlri.
* Choisissez le répertoire "content" puis "pages" puis "fr".
* Cliquez sur le bouton "+" au dessus de la liste des fichiers et choisissez "New file"
* Choisissez un nom cohérent terminant par ".md"
* Copiez le contenu récupéré sur la page en anglais et modifier l'attribut *lang*
* **L'attribut *slug* doit rester le même que sur la page en anglais**
* Traduisez les attributs qui vous semble nécessaires (Title, traduction, etc) et le contenu
  de la page.

#### Menu, sous-menu

Par défaut, toute nouvelle page est ajoutée au menu principal (en haut du site). 
Cependant, il est possible de rattacher une page donnée comme "sous-page" d'une autre.
Dans ce cas elle apparaîtra dans le menu déroulant et sur la page mère. Par exemple, les pages "Sage"
et "aSHIIP" sont des sous-pages de la page "Softwares".

La hiérarchie des pages se fait par les attributs : on donne la page parent comme attribut
`pageparent` à la sous-page. 

On peut aussi décider de créer une page indépendante qui n'apparaîtra pas dans le menu,
il suffit de lui rajouter `status: hidden` en attribut.

#### Membres associés à une page

L'attribut `members` permet d'attacher une liste de membre à une page donnée. Ils
s'afficheront sous la page (voir l'exemple sur les pages logiciels). On donne la liste
en séparant les membres par une virgule (Prénom Nom).

#### Articles associés à une page

Sur certaines pages, on peut souhaiter afficher une liste d'articles. C'est le cas
par exemple sur la page d'accueil ou sur la page Séminaire. Cela peut se faire
en donnant un attribut `include_category` à la page en question. On peut aussi 
rattacher les articles par tags avec l'attribut `include_tag`. Par défaut, la page
affichera 3 articles de la catégorie ou du tag sauf si l'attribut `number_articles`
est spécifié.

Exemples 

```
include_category: seminars
number_articles: 5
```

affichera les 5 derniers articles de la catégorie "seminars".

```
include_tag: Plateau seminar
number_articles: 2
```

affichera les deux derniers articles taggués "Plateau seminar".

#### Créer une nouvelle page

Avant de créer une nouvelle page, demandez-vous ou elle doit s'afficher : 

* dans le menu principal : comportement par défault
* En tant que sous-page d'une page existante : n'oubliez pas l'attribut *pageparent*
* En tant que page indépendante, hors du menu : rajouter un attribut `status:hidden`

Tout fichier créé dans le dossier *content/pages/ sera utilisé pour fabriquer une nouvelle 
page sur le site. Le plus simple est de s'inspirer d'une page existante. 

### Articles : Séminaires et news

Le site propose pour l'instant deux catégorie d'articles : les séminaires et les news.
Le fonctionnement des articles est très similaire à celui des pages, la syntaxe est la même
seuls quelques attributs diffèrent.

#### Ajouter un séminaire

Pour ajouter un séminaire, il faut créer un nouveau fichier dans *content/seminars/* :

* Cliquez sur le lien "Files" dans le menu de gauche de gitlri
* Allez dans *content/seminars/*
* Cliquez sur le bouton "+" au dessus de la liste des fichiers et choisissez "New File"
* Utilisez la date du seminaire comme nom de fichier, par exemple 2016-03-28.md pour 
  un séminaire du 28 mars 2016.
* Copiez le contenu ci-dessous et adaptez le
* Entrez un "commit message" (par exemple : "Ajout d'un nouveau séminaire") et 
  cliquez sur "Commit changes"

Voici un exemple de fichier type que vous pouvez copier :

```
Title: Titre du seminaire
date: 2016-03-28 14:30
slug: 2016-03-28-seminar
Authors: Prénom Nom
lang:en
institution: Instituion de l'auteur de séminaire
tags: Plateau seminar ou Team seminar
location: LIX
slide: document.pdf (à uploader dans content/documents/)

Résumé de l'exposé 
```

Le séminaire devrait apparaître sur la page d'accueil du site, sur la page séminaire 
et sur la page du séminaire du plateau si le tag "Plateau seminar" est utilisé.

#### Ajouter une news

La procédure est la même que pour la création d'un séminaire, simplement il faut créer
le fichier dans *content/news* et les attributs *location* et *institution* ne sont 
pas utiles. 


Le site utilise l'attribut "tags" pour lier la news à une thématique de recherche. Les
trois tag correspondant aux différentes thématiques sont : "network", "graph theory" et
"combinatorics". Voici un exemple type de fichier news.

```
Title: Title of the news
date: 2016-03-23
slug: 2016-03-23-my-news
Authors: Viviane Pons
lang: en
tags: combinatorics
img:some-image.jpg

Texte de l'article
```





